var Encore = require('@symfony/webpack-encore');
Encore
  // directory where all compiled assets will be stored
  .setOutputPath('var/webpack/')
  // what's the public path to this directory (relative to your project's document root dir)
  .setPublicPath('/')
  // empty the outputPath dir before each build
  .cleanupOutputBeforeBuild()
   
  // will output as app/Resources/webpack/server-bundle.js
  .addEntry('server-bundle','./assets/js/React/App/Startup/registration.js')
  // Add react preset
  .enableReactPreset()
.configureBabel(function (babelConfig) {
    // add additional presets
    // add additional presets
    const preset = babelConfig.presets.find(([name]) => name === "@babel/preset-env");
    if (preset !== undefined) {
        preset[1].useBuiltIns = "usage";
        preset[1].debug = true;
        preset[1].corejs = '3.0.0';
    }
  })
.enableSourceMaps(!Encore.isProduction())
;
// export the final configuration
module.exports = Encore.getWebpackConfig()