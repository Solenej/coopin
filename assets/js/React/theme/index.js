import { createMuiTheme } from '@material-ui/core/styles';

export default createMuiTheme({
  palette: {
    primary: {
      light: '#7b2292',
      main: '#7b2292',
      dark: '#7b2292',
      contrastText: '#fff'
    },
    secondary: {
      light: '#EF7400',
      main: '#EF7400',
      dark: '#EF7400',
      contrastText: '#fff'
    }
  },
  overrides: {
    MuiModal: {
      root: {
        // Has to be above everything else
        zIndex: 3000
      }
    },
    MuiOutlinedInput: {
      notchedOutline: {
        borderColor: '#EF7400 !important',
        borderStyle: 'solid !important',
        borderWidth: '1px !important',
        borderRadius: '4px !important'
      }
      // },
      // focused: {
      //   borderColor: '#780896',
      //   borderWidth: '2px'
      // }
    },
    // MuiButton: {
    //   root: {    MuiOutlinedInput: {
    //     notchedOutline: {
    //       borderColor: '#EF7400 !important',
    //       borderStyle: 'solid !important',
    //       borderWidth: '1px !important',
    //       borderRadius: '4px !important',
    //     },
    //     focused: {
    //       borderColor: '#EF7400',
    //       borderWidth: '2px'
    //     }
    //   },
    //     'text-transform': 'none',
    //     'border-radius': '2px',
    //     'box-shadow': 'none',
    //     fontSize: 13,
    //     color: '#fff',
    //     padding: '1em 2em 1em 2em'
    //   }
    MuiSelect: {
      select: {
        "&:focus": {
          backgroundColor: "none"
        }
      }
    }



  },
  typography: {
    useNextVariants: true,
    body1: {
      color: '#7b2292',
      margin: '1em',
      fontSize: 20
    },
    body2: {
      color: '#7b2292',
      fontSize: 13,
      fontStyle: 'italic'
    },
    h1: {
      color: '#7b2292',
      fontSize: '2em',
      fontWeight: 'bold'
    },
    h2: {
      fontSize: '2em',
      fontWeight: 'bold',
      textAlign: 'center'
    },
    h3: {
      fontSize: '1,6em',
      textAlign: 'left',
      fontWeight: 900,
      letterSpacing: 0.8
    },
    h4: {
      fontSize: 14.5,
      textAlign: 'left',
      fontWeight: 700,
      letterSpacing: 0.8
    },
    h5: {
      fontSize: '1,5em',
      textAlign: 'center'
    },
    h6: {
    },
    subtitle1: {
    },
    subtitle2: {
    },
    title3: {
    },
    subtitle3: {
    },
    button: {
    },
    display1: {
    },
     //fontFamily: [
       //'Verdana'
    //   'basier_circleregular',
    //   '-apple-system',
    //   'BlinkMacSystemFont',
    //   '"Segoe UI"',
    //   'Roboto',
    //   '"Helvetica Neue"',
    //   'Arial',
    //   'sans-serif',
    //   '"Apple Color Emoji"',
    //   '"Segoe UI Symbol"'
     //].join(',')
  }
});
