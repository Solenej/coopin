import React from 'react';
import { withRouter, Redirect } from 'react-router-dom';
import { CircularProgress } from '@material-ui/core';
import PropTypes from 'prop-types';


class Search extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            noMatch: false
        }
    }
    async componentDidMount() {
        const {
            username,
            jwt_token,
            loadData
        } = this.props;

        await loadData();

        const {
            session_id
        } = this.props

         if(!session_id) {
            this.conn = new WebSocket(`${this.props.basename}/search`);
            this.conn.onmessage = this.onMessage.bind(this);
        
            this.conn.onopen = () => {
                console.log('connexion ouverte');
    
                this.conn.send(JSON.stringify({
                    token: jwt_token,
                    username,
                    event: 'SEARCH'
                }))
            }
            this.conn.onclose = () => {
                console.log('connexion fermée'); 
            }
            
            setTimeout(
                function () {
                    this.setState({
                        noMatch: true
                    });
                }
                    .bind(this), 60000);
        }
    }
    
    componentWillUnmount() {
        if (this.conn) {
            this.conn.close();
        }
    }
    onMessage(data) {
        const {
            addStatusMatch
        } = this.props;

        const d = JSON.parse(data.data);
        const status = {
            event: d.event,
            session_id: d.session_id,
            chatSessionUserId: d.chatSessionUserId
        }
        addStatusMatch(status)
    }

    render() {
        const {
            matchOver
        } = this.props;
        const {
            noMatch
        } = this.state;
        return (
            <section className="search">
                {!matchOver && (
                    <div>
                        <img src={require('../../../../../images/recherche_en_cours_matching-removebg.png')} />
                        <CircularProgress />
                    </div>
                )}
                {matchOver  && (
                    <Redirect to={{ pathname: '/match', state: {noMatch: false}}} />
                )}
                {noMatch  && (
                    <Redirect to={{ pathname: '/match', state: {noMatch: true} }} />
                )}
            </section>
        );
    }

}

Search.propTypes = {
    matchOver: PropTypes.bool

  };

  Search.defaultProps = {
    matchOver: false
  };


export default withRouter(Search);
