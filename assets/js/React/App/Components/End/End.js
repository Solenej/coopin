import React from 'react';
import { Typography, withStyles, Button } from '@material-ui/core';
import PropTypes from 'prop-types';

class End extends React.Component {
    constructor(props) {
    super(props);
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        const {
            history
        } = this.props;
        
        history.push('/login')
    }

    render (){
        const { classes } = this.props;
        return <div>
                <div className="end-centrage">
                    <Typography className={classes.title1} variant="h2" color="secondary">A bientôt</Typography>
                    <Typography className={classes.title1} variant="h2" color="primary"> pour de nouvelles aventures</Typography>
                    <div className="end-img">
                        <img className="end-resize" src={require('../../../../../images/hearts.svg')}></img>
                        <Button onClick={() => this.handleClick()}><img className="end-resize position" src={require('../../../../../images/speech-bubbles.png')}></img></Button>
                    </div>
                </div>
               </div>
    }
}

const style = theme => ({
    title1: {
        margin: '10px',
        [theme.breakpoints.down(560)]: {
            fontSize: '1em',
        }
    }
})

End.propTypes = {
    classes: PropTypes.object.isRequired,
  };

export default withStyles(style)(End);
