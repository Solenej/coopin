import React from 'react';
import PropTypes from 'prop-types';
import {
  Typography,
  withStyles,
  Button,
  TextField,
  MenuItem,
  InputAdornment,
  IconButton,
  FormControlLabel,
  Checkbox,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Paper
} from '@material-ui/core';
import {
  Visibility,
  VisibilityOff
} from '@material-ui/icons';

class Admin extends React.Component {
  constructor(props) {
    super(props);
    this.handleOnClick = this.handleOnClick.bind(this);
    this.handleClickShowPassword = this.handleClickShowPassword.bind(this);
    this.state = {
      NewGroup: false,
      NewUser: false,
      NewText: false,
      NewTheme: false,
      text: '',
      _nameGroup: '',
      _codeAccess: '',
      _nameTheme: '',
      showPassword: false,
      data: false,
      _checkedAdmin: false
    }

  }

  componentDidMount() {
    const {
      loadData
    } = this.props;

    loadData();
  }


  handleOnClick(type) {
    if (type === 'NewGroup') {
      this.setState(() => ({
        NewGroup: true,
        NewUser: false,
        NewText: false,
        NewTheme: false,
        data: false
      })
      )
    } else if (type === 'NewUser') {
      this.setState({
        NewGroup: false,
        NewUser: true,
        NewText: false,
        NewTheme: false,
        data: false
      })
    } else if (type === 'NewTheme') {
      this.setState({
        NewGroup: false,
        NewUser: false,
        NewText: false,
        NewTheme: true,
        data: false
      })
    } else if (type === 'data') {
      const {
        exportData
      } = this.props;

      exportData();
      this.setState({
        NewGroup: false,
        NewUser: false,
        NewText: false,
        NewTheme: false,
        data: true
      })
    } else {
      this.setState({
        NewGroup: false,
        NewUser: false,
        NewText: true,
        NewTheme: false,
        data: false
      })
    }
  }

  changeValue(e, type) {
    const {
      _checkedAdmin
    } = this.state;
    const value = e.target.value;
    const nextState = {};
    nextState[type] = value;
    if(type === '_checkedAdmin') {
      this.setState({
        _checkedAdmin: !_checkedAdmin
      })
    } else {
      this.setState(nextState);
    }


  }

  handleClickShowPassword() {
    this.setState(state => ({ showPassword: !state.showPassword }));
  };

  submit(e) {
    e.preventDefault();

    const {
      onSubmitGroup,
      onSubmitUser,
      onSubmitTheme,
      onSubmitText
    } = this.props;
    const {
      NewGroup,
      NewUser,
      NewTheme,
      _nameGroup,
      _codeAccess,
      _nameTheme,
      text,
      _checkedAdmin
    } = this.state;
    if (NewGroup) {
      onSubmitGroup(_nameGroup);
    } else if (NewUser) {
      onSubmitUser(_nameGroup, _codeAccess, _checkedAdmin);
    } else if (NewTheme) {
      onSubmitTheme(_nameTheme);
    } else {
      onSubmitText(text);
    }
    this.setState(() => ({
      _nameGroup: '',
      _codeAccess: '',
      _nameTheme: '',
      _checkedAdmin: false
    }))
  }
  render() {
    const {
      classes,
      listGroup,
      listTheme,
      textExplication,
      errorGroup,
      errorText,
      errorUser,
      errorTheme,
      listUserGroup,
      errorData
    } = this.props;
    const {
      NewGroup,
      NewUser,
      _nameGroup,
      _codeAccess,
      _nameTheme,
      showPassword,
      NewText,
      NewTheme,
      data,
      _checkedAdmin
    } = this.state;
    return (
      <div className="admin-container">
        <Typography className={classes.adminTitle} variant="h1">Espace administration</Typography>
        <div className="admin-link">
          <Button onClick={() => { this.handleOnClick('NewGroup') }}><Typography className={classes.adminSubTitle}>Créer un groupe</Typography></Button>
          <Button onClick={() => { this.handleOnClick('NewUser') }}><Typography className={classes.adminSubTitle}>Créer un utilisateur</Typography></Button>
          <Button onClick={() => { this.handleOnClick('NewText') }}><Typography className={classes.adminSubTitle}>Créer un texte d'accueil</Typography></Button>
          <Button onClick={() => { this.handleOnClick('NewTheme') }}><Typography className={classes.adminSubTitle}>Créer un thème hashtag</Typography></Button>
          <Button onClick={() => { this.handleOnClick('data') }}><Typography className={classes.adminSubTitle}>Exporter les données</Typography></Button>
        </div>

        {NewGroup && (
          <div className="admin-group-container">
            <form className="admin-form" method="POST" onSubmit={this.submit.bind(this)} autoComplete="off">
              <div className={classes.adminContainerInput}>
                <TextField
                  className={classes.textfield}
                  onChange={e => this.changeValue(e, '_nameGroup')}
                  name='_nameGroup'
                  value={_nameGroup}
                  InputLabelProps={{
                    classes: {
                      root: classes.cssLabel
                    },
                  }}
                  InputProps={{
                    classes: {
                      input: classes.input
                    },
                  }}
                  label="Nom du groupe"
                  variant="outlined"
                />
              </div>

              {errorGroup !== "" && <Typography className={classes.error}>{errorGroup}</Typography>}

              <Button
                size='large'
                className={classes.button}
                variant="contained"
                color="primary"
                type="submit"
                disabled={_nameGroup === ''}>
                Créer
              </Button>
            </form>
            <Typography color="secondary" variant="body1" className={classes.listTitle}>Liste des groupes existants</Typography>
            <div className="admin-list-group">
              {listGroup.map(group => {
                return (
                  <li key={group.label} className={classes.listGroupItem}> {group.label} </li>
                )
              })}
            </div>
          </div>
        )}

        {NewUser && (
          <div className="admin-group-container">
            <form className="admin-form" method="POST" onSubmit={this.submit.bind(this)} autoComplete="off">
              <div className={classes.adminContainerInput}>
                <TextField
                  id="outlined-select-currency"
                  select
                  label="Nom du groupe"
                  name='_nameGroup'
                  value={_nameGroup}
                  onChange={e => this.changeValue(e, '_nameGroup')}
                  InputLabelProps={{
                    classes: {
                      root: classes.cssLabel
                    },
                  }}
                  InputProps={{
                    classes: {
                      input: classes.input
                    },
                  }}
                  SelectProps={{
                    MenuProps: {
                      className: classes.menu,
                    },
                  }}
                  helperText="S'il vous plaît choisissez un groupe"
                  margin="normal"
                  variant="outlined"
                >
                  {listGroup.map((option, key) => (
                    <MenuItem key={`option.value${key}`} value={option.value}>
                      {option.label}
                    </MenuItem>
                  ))}
                </TextField>
                <TextField
                  onChange={e => this.changeValue(e, '_codeAccess')}
                  className={classes.textfield}
                  name='_codeAccess'
                  type={showPassword ? 'text' : 'password'}
                  value={_codeAccess}
                  InputLabelProps={{
                    classes: {
                      root: classes.cssLabel
                    }
                  }}
                  InputProps={{
                    classes: {
                      input: classes.input
                    },
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton
                          className={classes.buttonView}
                          aria-label="Toggle password visibility"
                          onClick={this.handleClickShowPassword}
                        >
                          {showPassword ? <VisibilityOff /> : <Visibility />}
                        </IconButton>
                      </InputAdornment>
                    )
                  }}
                  label="Code d'accès"
                  variant="outlined"
                />
              </div>
              <FormControlLabel
                control={
                  <Checkbox checked={_checkedAdmin} onChange={(e) => this.changeValue(e, '_checkedAdmin')} value="_checkedAdmin" name="_checkedAdmin" color="primary"/>
                }
                label="Admin"
              />
              {errorUser !== "" && <Typography className={classes.error}>{errorUser}</Typography>}
              <Button
                size='large'
                className={classes.button}
                variant="contained"
                color="primary"
                type="submit"
                onClick={() => this.submit.bind(this)}
                disabled={_codeAccess.length < 3 || _nameGroup === ""}
              >
                Créer
                    </Button>
            </form>
            <Typography color="secondary" variant="body1" className={classes.listTitle}>Liste des utilisateurs existants</Typography>

            <Paper className={classes.paper}>
              <Table className={classes.table}>
                <TableHead>
                  <TableRow>
                    <TableCell>Codes accès</TableCell>
                    <TableCell align="center">Groupes</TableCell>
                    <TableCell align="center">Roles</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {listUserGroup.map(list => (
                    <TableRow key={list.username}>
                      <TableCell component="th" scope="row">{list.username}</TableCell>
                      <TableCell align="center">
                        {list.libelle}
                      </TableCell>
                      <TableCell align="center">{list.roles === '[\"ROLE_ADMIN\"]' ? list.roles.substring(2,12) : list.roles.substring(2,11)}</TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </Paper>
            {/* <div className="admin-list-group">
              {listUsers.map(user => {
                return (
                  <li key={user.label} className={classes.listGroupItem}> {user.label} </li>
                )
              })}
            </div> */}
          </div>
        )}

        {NewText && (
          <form className="admin-form, newText" method="POST" onSubmit={this.submit.bind(this)} autoComplete="off">
            <div className={classes.adminContainerInput}>
              <TextField
                className={classes.textFieldNewText}
                color="secondary"
                multiline
                name="text"
                rows="7"
                variant="outlined"
                defaultValue={textExplication}
                placeholder="Ecrivez votre texte"
                onChange={e => this.changeValue(e, 'text')}
                InputProps={{
                  classes: {
                    input: classes.input
                  },
                }}
              />
              {errorText !== "" && <Typography className={classes.error}>{errorText}</Typography>}
              <Button
                size='large'
                className={classes.button}
                variant="contained"
                color="primary"
                type="submit"
              >
                Créer
                    </Button>
            </div>
          </form>
        )}

        {NewTheme && (
          <div className="admin-group-container">
          <form className="admin-form, newText" method="POST" onSubmit={this.submit.bind(this)} autoComplete="off">
            <div className={classes.adminContainerInput}>
            <TextField
                  className={classes.textfield}
                  onChange={e => this.changeValue(e, '_nameTheme')}
                  name='_nameTheme'
                  value={_nameTheme}
                  InputLabelProps={{
                    classes: {
                      root: classes.cssLabel
                    },
                  }}
                  InputProps={{
                    classes: {
                      input: classes.input
                    },
                  }}
                  label="Nom du thème"
                  variant="outlined"
                />
              {errorTheme !== "" && <Typography className={classes.error}>{errorTheme}</Typography>}
              <Button
                size='large'
                className={classes.button}
                variant="contained"
                color="primary"
                type="submit"
              >
                Créer
                    </Button>
            </div>
          </form>
          <Typography color="secondary" variant="body1" className={classes.listTitle}>Liste des thèmes hashtags existants</Typography>
             <div className="admin-list-group">
              {listTheme.map(theme => {
                return (
                  <li key={theme.nom} className={classes.listGroupItem}> {theme.nom} </li>
                )
              })}
            </div>
          </div>
        )}
        {data && (
          errorData ?
            <Button
              size='large'
              className={classes.button}
              variant="contained"
              color="primary"
              href="/../../download/exportCoopin.zip"
            >
              Télécharger
                  </Button>
            :
            <Typography className={classes.error}>Il y a eu un problème avec l'exportation, veuillez réessayer s'il vous plaît.</Typography>
        )}
      </div>
    )
  }
}

const styles = () => ({
  adminTitle: {
    margin: 20
  },
  adminSubTitle: {
    marginLeft: 10,
    marginRight: 10,
    cursor: 'pointer'
  },
  adminLink: {
    '&:hover': {
      textDecoration: 'none'
    }
  },
  listTitle: {
    margin: 0,
    marginTop: 5,
  },
  listGroupItem: {
    color: '#7b2292',
    margin: 10,
    width: 120,
    fontSize: 12
  },
  buttonView: {
    padding: 0
  },
  adminContainerInput: {
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column'
  },
  cssLabel: {
    color: '#EF7400',
    // '&$cssFocused': {
    //   color: '#EF7400',
    // },
  },
  input: {
    color: '#EF7400'
  },
  button: {
    width: 150,
    margin: '20px auto'
  },
  textfield: {
    marginTop: 20
  },
  selectOutlineInput: {
    paddinRight: 20,
    color: '#EF7400',
    '&:focus': {
      backgroundColor: 'white'
    }
  },
  textFieldNewText: {
    width: 290
  },
  error: {
    color: 'red',
    marginBottom: 20,
    textAlign: 'center'
  },
  paper: {
    marginTop: 20
  }
});

Admin.propTypes = {
  classes: PropTypes.shape({}).isRequired,
  loadData: PropTypes.func,
  listGroup: PropTypes.array,
  textExplication: PropTypes.string,
  exportData: PropTypes.func

};

Admin.defaultProps = {
  loadData: () => { },
  listGroup: [],
  textExplication: '',
  exportData: () => { }
};

export default withStyles(styles)(Admin);
