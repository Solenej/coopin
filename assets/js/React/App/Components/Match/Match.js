import React from 'react';
import { Link } from 'react-router-dom';
import { Button, Typography } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

const styles = {
    typo: {
        fontSize: 20,
        marginLeft: "1em"
    }
};

class Match extends React.Component {
    render() {
        const {
            classes,
            matched
        } = this.props;
        const noMatch = this.props.location ? this.props.location.state.noMatch : false;
        return (
            <section className="match">
                {matched && (
                    <div>
                        <section className="match-go">
                            <img src={require('../../../../../images/coopin-full-owl.png')}></img>
                            <Typography variant="body2" className={classes.typo}>Je t'ai trouvé une Coopin</Typography>
                        </section>
                        <Button variant="contained" color="primary" className="button-go" component={Link} to="/chatRoom">On y va ?</Button>
                    </div>
                )}
                {noMatch && (
                    <div>
                        <section>
                            <img src={require('../../../../../images/coopin-full-owl-sad.png')} />
                            <Typography variant="body1" >Aucune <strong>coopincoach</strong> n'est diponible</Typography>
                        </section>
                        <section className="nomatch-retry">
                            <img src={require('../../../../../images/hearts.svg')}></img>
                            <Typography variant="body1" >Re-tente ta chance !</Typography>
                            <Link to="/search"><img src={require('../../../../../images/speech-bubbles.png')} /></Link>
                        </section>
                    </div>
                )}
            </section>
        );
    }
}

export default withStyles(styles)(Match);
