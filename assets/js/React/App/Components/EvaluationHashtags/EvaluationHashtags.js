import React from 'react';
import {Link} from 'react-router-dom';
import { Typography, Button } from '@material-ui/core';
import Hashtag from '../Basics/Hashtag/Hashtag';
import HashtagInput from '../Basics/HashtagInput/HashtagInput';
import PropTypes from 'prop-types';

class EvaluationHashtags extends React.Component {


    constructor(props) {
        super(props);
        
        this.state = {
            value:'',
            hashtags: props.hashtagsObject,
            otherHashtags: [null, null, null, null, null, null]
        }
    }

    componentDidMount(){
        const { themes } = this.props;
        
        themes.forEach(hashtag => {
            this.props.hashtagsObject[hashtag.nom] = false;
        });

        this.setState({
            hashtags: this.props.hashtagsObject
        });
    }

    toggleHashtag(key) {
        this.setState(state => ({ hashtags: { ...state.hashtags, [key]: !state.hashtags[key] } }))
    }

    changeValue(e, i) {
        const value = e.target.value;
        const otherHashtags = [...this.state.otherHashtags]
        otherHashtags.splice(i, 1, value);
        this.setState({otherHashtags});
      }

    render() {
        const {
            handleSubmit
        } = this.props;
        const {
            hashtags,
            otherHashtags
        } = this.state;
        return <section className="evaluation-hashtags">
            <Typography variant="body1">Avec ma Coopin, nous avons parlé de...</Typography>
            <section className="e-hashtags-list">
                {Object.keys(this.state.hashtags).map(
                    (label, i) => <Hashtag checked={this.state.hashtags[label]}
                    toggleHashtag={() => this.toggleHashtag(label)}
                    key={i} index={i} label={label} />
                )}
            </section>
            <Typography variant="body1">Autre chose ? Dis nous quoi :</Typography>
            <section className="e-hashtags-input">
                {[0,1,2,3,4,5].map(i => <HashtagInput key={i} index={i} onChange={e => this.changeValue(e, i)}/>)}
            </section>
            <Button
                variant="contained"
                color="primary"
                onClick={() => handleSubmit(hashtags,otherHashtags)}
                component={Link}
                to="/evaluation-feelings"
            >
            Suivant
            </Button>
        </section>
    }
}

EvaluationHashtags.propTypes = {
    loadThemes: PropTypes.func,
    themes: PropTypes.array.isRequired
  
  };
  
  EvaluationHashtags.defaultProps = {
    loadThemes: () => {}
  };

export default EvaluationHashtags;