import React from 'react';
import { Link } from 'react-router-dom';
import { Typography, Button, TextField } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

const styles = {
    typo1: {
        fontSize: 14,
        alignSelf: "flex-start",
        margin: 0,
        marginBottom: "1em"
    },
    typo: {
        marginTop: 20,
        marginBottom: 40
    },
    typo2: {
        fontSize: 13,
        fontStyle: 'normal',
        fontWeight: 500
    },
    exitButton: {
        width: 270,
        alignSelf: 'flex-end'
    },
    input: {
        color: '#7b2292'
    }
}

class Exit extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            disabled: true,
            com: '',
            disable: false
        }
        this.comment = "";
        this.onCommentChange = this.onCommentChange.bind(this);
        this.handleComment = this.handleComment.bind(this);
    }

    componentDidMount() {
        const {
            session_id,
            eventOther,
            deconnexionChat,
            loadThemes
        } = this.props;

        loadThemes();

        deconnexionChat();
        
        if (eventOther !== 'STOP'){
            deconnexionChat();
            this.conn = new WebSocket(`${this.props.basename}/chatSession/${session_id}`);
            this.conn.onmessage = this.onMessage.bind(this);
            this.conn.onopen = () => {
                console.log('connexion ouverte');
            }

            this.conn.onclose = () => {
                console.log('connexion fermée');
            }
        }

    };

    componentWillUnmount() {
        if (this.conn) {
            this.conn.close();
        }
    }

    onMessage(data) {
        const {
            session_id,
            chatSessionUserId,
            addMessage,
            addEventOther
        } = this.props;

        const d = JSON.parse(data.data);

        const chat = {
            session_id,
            chatSessionUserId,
            username: d.username,
            texte: d.message
        };
        addMessage(chat);
        addEventOther(d.event)
    }

    handleComment(comment) {
        const {
            addComment
        } = this.props;
        try {
            this.setState({
                disable: true
            })
        } catch (err) {
            console.log({err});
            throw err;
        }
        addComment(comment);
    };


    onCommentChange(e){
        this.comment = e.target.value;
        this.setState({
            com: this.comment
        })
        if(this.comment.length !== 0){
            this.setState({
                disabled: false
            });
        }else{
            this.setState({
                disabled: true
            });
        }
        const {
            eventOther,
            addEvent,
            jwt_token,
            username,
        } = this.props;

        if (eventOther !== 'STOP') {
            this.conn.send(JSON.stringify({
                token: jwt_token,
                username,
                event: 'STOP'
            }))
            this.setState({
                disable: true
            })
            addEvent('STOP')
        }
    }

    render() {
        const {
            classes,
            quit
        } = this.props;
        const {
            com,
            disable,
            disabled
        } = this.state;
        const startChat = this.props.location && this.props.location.state && this.props.location.state.startChat ;
        return (
            <section className="exit">
                <section className="exit-text-image">
                    {startChat &&
                        <section className="exit-text">
                            <Typography variant="h3" className={classes.typo} color="secondary">Oups, tu n'as pas évalué ta dernière session de chat...</Typography>
                        </section>

                    }
                    {(!quit && !startChat) &&
                        <section className="exit-text">
                            <Typography variant="h3" className={classes.typo} color="secondary">Tu nous quittes déjà ?</Typography>
                            <Typography variant="h4" color="primary">Si c'est une erreur,</Typography>
                            <Typography variant="h4" color="primary">clique vite pour y retourner..</Typography>
                            <div className="exit-img">
                            <Link to="/chatRoom" className={(disable || startChat) ? 'disabled-link' : "" }><img src={require('../../../../../images/speech-bubbles.png')} className="exit-text-bubbles"/></Link>
                            </div>
                        </section>
                    }
                    {(quit && !startChat) &&
                        <section className="exit-text">
                            <Typography variant="h3" className={classes.typo} color="secondary">Oups, ta copine est partie...</Typography>
                        </section>
                    }
                    <img src={require("../../../../../images/coopin-full-owl-sad.png")} className="exit-image" />
                </section>
                <section className="exit-textfield-text">
                    {(!quit && !startChat) &&
                        <div>
                            <Typography variant="body2" className={classes.typo1}>Ce n'est pas une erreur ?</Typography>
                            <Typography variant="body2" className={classes.typo2} color="secondary">S'il te plait, dis-nous pourquoi ?</Typography>
                        </div>
                    }
                    {(quit || startChat) &&
                        <div>
                            <Typography variant="body2" className={classes.typo2} color="secondary">Dis nous ce que tu en as pensé.</Typography>
                        </div>
                    }
                    <form className="exit-containerForm">
                        <TextField
                            color="secondary"
                            multiline
                            rows="5"
                            variant="outlined"
                            placeholder="Ecris ton message"
                            onChange={(e) => this.onCommentChange(e)}
                            InputProps={{
                                classes: {
                                    input: classes.input
                                },
                            }}
                        />
                        <Button className={classes.exitButton} onClick={() => this.handleComment(com)} color="primary" disabled={disabled} component={Link} to="/evaluation-hashtags">Nous avons encore besoin de toi</Button>
                    </form>
                </section>
            </section>
        )
    }
}

export default withStyles(styles)(Exit);
