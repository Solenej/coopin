import React, { Fragment } from 'react';
import { Link, withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';

const Header = (props) => {
    const { pathname } = props.location
    const noHeader = pathname === "/";

    if(noHeader) {
      return <Fragment></Fragment>
    }
    return (
      <div className="header">
        <Link to="/">
          <img className="header-img" src={require('../../../../../images/image1.svg')} />
        </Link>
      </div>
    )
}

Header.propTypes = {
  button: PropTypes.bool,
  play: PropTypes.bool
}

Header.defaultProps = {
  button: false,
  play: false
}
export default withRouter(Header);
