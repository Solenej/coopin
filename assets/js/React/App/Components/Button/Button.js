import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import { Icon, Button, withStyles } from '@material-ui/core';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const ButtonChatRoom = (props) => {
  const {
    classes,
    onClickPause,
    onClickPlay,
    onClickStop,
    not,
    play,
    pause,
    stop
  } = props;

  return (
    <div className={classNames({ ["button"]: pause, ["buttonPlay"]: play, ["buttonStop"]: stop})}>
      {pause && !not && (
        <Button onClick={onClickPause} className={classes.button}><Icon fontSize="large" color="secondary">pause</Icon></Button>
      )}
      {play && !not && (
        <Button onClick={onClickPlay} className={classes.button}><Icon fontSize="large" color="secondary">play_arrow</Icon></Button>
      )}
      {(pause || play || stop) && (
        <Button onClick={onClickStop} className={classes.button} component={Link} to="/exit"><Icon fontSize="large" color="secondary">stop</Icon></Button>
      )}
    </div>
  )
}

const styles = () =>({
  button: {
    borderRadius: 50
  }
});

ButtonChatRoom.propTypes = {
  onClickPause: PropTypes.func,
  onClickPlay: PropTypes.func,
  onClickStop: PropTypes.func
}
ButtonChatRoom.defaultProps = {
  classNamePause: '',
  onClickPause: () => {},
  onClickPlay: () => {},
  onClickStop: () => {}
}


const styleButton = withStyles(styles)(ButtonChatRoom);
export default withRouter(styleButton);
