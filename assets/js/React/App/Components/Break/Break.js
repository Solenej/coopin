import ButtonChatRoom from '../Button/Button';
import React from 'react';
import { withStyles } from '@material-ui/core';
import PropTypes from 'prop-types';


class Break extends React.Component {
    render() {
        const {
            classes
        } = this.props;
        return (
            <div className="break-img">
                <ButtonChatRoom
                  classNamePause={classes.pauseButton}
                  classNameStop={classes.stopButton}
                  onClickPlay={(e) => this.submitMessage('PLAY', e)}
                  onClickStop={(e) => this.submitMessage('STOP', e)}
                />
                <img className="break-img-1" src={require('../../../../../images/chouette_en_standby-removebg.png')}></img>
                <img className="break-img-2" src={require('../../../../../images/speech-bubbles.png')}></img>
            </div>
        )
    }
}

const styles = () => ({
    pauseButton: {
      borderRadius: 50,
      position: 'absolute',
      zIndex: 3,
      top: -185,
      left: -30
    },
    stopButton: {
      borderRadius: 50,
      position: 'absolute',
      zIndex: 3,
      top: -185,
      right: -30
    }
  });

  Break.propTypes = {
    classes: PropTypes.shape({}).isRequired
};
export default withStyles(styles)(Break);