import React from 'react';
import {Link} from 'react-router-dom';
import { Button, Typography } from '@material-ui/core';


class Home extends React.Component {

    render (){

        return (
                <div className="pdg">
                    <div className="pdg_container">
                        <Typography className="pdg_title" variant="h1">Bienvenue</Typography>
                        <div className="pdg_logo">
                            <img src={require('../../../../../images/Coopin entraide.svg')}></img>
                        </div>
                        <Button variant="contained" color="primary" component={Link} to="/login">C'est par ici ?</Button>
                    </div>
                </div>

        );
    }
}

export default (Home);
