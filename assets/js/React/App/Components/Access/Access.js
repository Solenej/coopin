import React from 'react';
import {
  Typography,
  withStyles,
  TextField,
  Button,
  CircularProgress,
  Link
} from '@material-ui/core';
import PropTypes from 'prop-types';


class Access extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      username: props.last_username,
      password:'',
      submitted: false
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    const {
      loadData
    } = this.props;

    loadData();
  }

  isDisabled() {
    let usernameIsValid = false;
    let passwordIsValid = false;

    if (this.state.username === "" || !this.state.username) {
        this.setState({
          usernameValid: null
        });
    } else {
        if (this.state.username.length >= 3) {
          usernameIsValid = true
            this.setState({
              usernameValid: null
            });
        } else {
            this.setState({
              usernameValid: "Le pseudo n'est pas au bon format"
            });
        }
    }

    if (this.state.password === "" || !this.state.password) {
        this.setState({
          passwordValid: null
        });
    } else {
        if (this.state.password.length >= 3) {
            passwordIsValid = true;
            this.setState({
              passwordValid: null
            });
        } else {
            this.setState({
              passwordValid: "Votre password n'est pas au bon format"
            });
        }
    }

    if (usernameIsValid && passwordIsValid) {
        this.setState({
            disabled: false
        });
    }
  }

  handleChange(e, type) {
    const value = e.target.value;
    const nextState = {};
    nextState[type] = value;
    this.setState(nextState, () => {
        this.isDisabled()
    });
  }

  async handleSubmit(event) {
      event.preventDefault();
      this.setState({ submitted: true });
      const { username, password } = this.state;
      const { logIn } = this.props;
      if (username && password) {
        logIn(username, password);
      }
   }

  render() {
    const {
      classes,
      requiredUsername,
      requiredPassword,
      textExplication,
      loggingIn,
      errorType
     } = this.props;
    const {
      usernameValid,
      passwordValid,
      disabled
    } = this.state;
    return (
      <div className={classes.accessContainer}>
        <Typography variant="h3" className={classes.accessH3} align="center" color="primary">Accès personnalisé & anonymisé</Typography>
          <form method="POST" className={classes.accessContainerButtons} onSubmit={this.handleSubmit} autoComplete="off">
            {this.state.error && <div>{this.state.error}</div>}
            <div className={classes.accessContainerButton}>
              <TextField
                required={requiredUsername}
                onChange={e => this.handleChange(e, 'username')}
                error={errorType !== '' || usernameValid !== null}
                name='_username'
                InputLabelProps={{
                  classes: {
                    root: classes.cssLabel
                  },
                }}
                InputProps={{
                  classes: {
                    input: classes.input
                  },
                }}
                FormHelperTextProps={{
                  classes: {
                  }
                }}
                label="Pseudo"
                variant="outlined"
              />
            </div>

            <div className={classes.accessContainerButton}>
              <TextField
                required={requiredPassword}
                type="password"
                name='_password'
                error={errorType !== '' || passwordValid !== null}
                onChange={e => this.handleChange(e, 'password')}
                InputLabelProps={{
                  classes: {
                    root: classes.cssLabel
                  },
                }}
                InputProps={{
                  classes: {
                    input: classes.input
                  },
                }}
                label="Code d'accès"
                variant="outlined"
              />
            </div>
            <Button
              size='large'
              className={classes.button}
              variant="contained"
              color="primary"
              type="submit"
              disabled={disabled}
            >
            connexion
            </Button>
            {errorType !== "" && <Typography className={classes.accessError}>Tu ne parviens pas à te connecter ?</Typography>}
            {loggingIn &&
                            <CircularProgress />
                        }
          </form>
          {(errorType === "") && (
              <Typography className={classes.accessText}>{textExplication}</Typography>
          )}
          {(errorType !== "") && (
            <div>
              <Typography
                variant="body2"
                color="primary"
              >
                Si tu fais partie du programme de tests, envoyez un mail:
              </Typography>
              <Link
                className={classes.accessTypo}
                underline="hover"
                variant="body2"
                color="secondary"
                href="mailto:tests@coopincoach.com"
              >
                <Typography color="secondary">tests@coopincoach.com></Typography>
              </Link>
              <Typography
                className={classes.accessTypo}
                variant="body2" color="primary"
              >
                en précisant tes noms et prénoms
              </Typography>
              <img className="accessImg" src={require('../../../../../images/hearts.svg')} />
              <Typography
                variant="body2"
                color="primary"
              >
                Si tu ne fais pas partie du programme, envoie un mail:
              </Typography>
              <Link
                className={classes.accessTypo}
                underline="hover"
                variant="body2"
                color="secondary"
                href="mailto:ensavoirplus@coopincoach.com"
              >
                <Typography color="secondary">ensavoirplus@coopincoach.com</Typography>
              </Link>
              <Typography
                className={classes.accessTypo}
                variant="body2"
                color="primary"
              >
                pour être prévenue de nos avancées et du lancement de nos services
              </Typography>
            </div>
          )
           }
      </div>
    )
  };
}

const stylesAccess = () => ({
  accessContainer: {
    margin: '90px auto 0px auto',
    width: 330
  },
  accessContainerButtons: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    flexWrap: 'wrap',
    margin: '30px 20px 0px 20px'
  },
  accessContainerButton: {
    margin: 10,
  },
  input: {
    color: '#EF7400'
  },
  accessTypo: {
    textAlign: 'center'
  },
  accessText: {
    textAlign: 'center',
    marginTop: 30,
    border: '1px #A7A6A6 solid'
  },
  accessError: {
    color: 'red',
    marginBottom: 20
  },
  button: {
    width: 150,
    margin: 30
  },
  cssLabel: {
    color: '#EF7400',
    // '&$cssFocused': {
    //   color: '#EF7400',
    // },
  }
});

Access.propTypes = {
  classes: PropTypes.object.isRequired,
  requiredPassword: PropTypes.bool,
  requiredUsername: PropTypes.bool,
  textExplication: PropTypes.string,
  errorType: PropTypes.string,
  loggingIn: PropTypes.bool

};

Access.defaultProps = {
  errorType: "",
  username_error_text: "Oops, il y a un problème!!!",
  password_error_text:  "",
  requiredPassword: false,
  requiredUsername: false,
  loggingIn: false,
  textExplication: ''
}

const AccessStyle = withStyles(stylesAccess)(Access);
export default AccessStyle;
