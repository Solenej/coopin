import React from 'react';
import {Link} from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import { Typography, Button } from '@material-ui/core';
import Feeling from '../Basics/Feeling/Feeling';

const styles = {
    clicked: {
        opacity: 1
    },
    unclicked: {
        opacity: 0.5
    },
    orange: {
        color: '#EF7400'
    }
}

class EvaluationFeelings extends React.Component {
    constructor(props, context) {
        super(props, context);
       
        this.state = {
            feeling: null,
            feelings: [false, false, false, false, false, false],
            clicked: false
        }
    this.toggleFeeling = this.toggleFeeling.bind(this)
    }

    toggleFeeling(key) {
        this.setState({
            feelings: [false, false, false, false, false, false]
        });
        this.setState(state => ({ feelings: { ...state.feelings, [key]: !state.feelings[key] } }));
        this.setState({
            feeling: parseInt(key),
            clicked: true
        })       
    }

    render() {
        const {
            classes,
            handleSubmitFeeling
        } = this.props;
        const { 
            feeling
        } = this.state;

        return <section className="evaluation-feelings">
            <Typography variant="body1">Pendant les <strong className={classes.orange}>échanges</strong> je me suis sentie...</Typography>
            <section className="e-feelings-list">
            {Object.keys(this.state.feelings).map(
                    (i) => <Feeling clicked={this.state.feelings[i]}
                    toggleFeeling={() => this.toggleFeeling(i)}
                    key={i} index={i} />
                )}
            </section>
            <Button
                color="secondary"
                component={Link}
                to="/stars"
                disabled={!this.state.clicked}
                onClick={() => handleSubmitFeeling(feeling)}
            >
            C'est presque fini
            </Button>
        </section>
    }
}

export default withStyles(styles)(EvaluationFeelings);