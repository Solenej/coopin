import React from 'react';
import { Typography } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

const styles = {
    label: {
        cursor: "pointer"
    },
    checked: {
        opacity: 1,
        fontWeight: 'bold'
    },
    unchecked: {
        opacity: 0.5
    },
    orange: {
        color: '#EF7400'
    }
}

class Hashtag extends React.Component {
    render() {
        const {
            checked,
            toggleHashtag,
            classes,
            index
        } = this.props;
        return (
            <Typography
                className={(checked ? classes.checked : classes.unchecked) + " " + classes.label + " " + (index % 2 === 0 ? classes.orange : "")}
                onClick={() => toggleHashtag()}
                variant="body1" >#{this.props.label}
            </Typography>
        )
    }
}

export default withStyles(styles)(Hashtag);