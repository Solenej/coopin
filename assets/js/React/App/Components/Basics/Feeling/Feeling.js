import React from 'react';
import { withStyles } from '@material-ui/core/styles';

const styles = {
    label: {
        cursor: "pointer"
    },
    clicked: {
        opacity: 1,
    },
    unclicked: {
        opacity: 0.5
    }
}

class Feeling extends React.Component {

    render() {
        const {
            clicked,
            toggleFeeling,
            classes,
            index
        } = this.props;
        return <img className={(clicked ? classes.clicked : classes.unclicked) + " e-feelings-image"}
            onClick={() => toggleFeeling()}
            src={require(`../../../../../../images/smiley-${index}.svg`)} />
    }
}

export default withStyles(styles)(Feeling);