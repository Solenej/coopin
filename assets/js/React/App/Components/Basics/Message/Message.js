import React from 'react';
import { Typography, withStyles } from '@material-ui/core';

const Message = ({ chat, user }) =>  (
        <li className={`chat ${user === chat.username ? "right" : "left"}`}>
            {user === chat.username ?
                <Typography align='right' color="primary">Moi : </Typography> : ""
                /* <Typography color="secondary" >{chat.username} : </Typography> */
            }
            {/* {user !== chat.username
            && <img src={chat.img} alt={`${chat.username}'s profile pic`} />
            } */}
            {chat.texte}
            {/* <p className="messageDate">{chat.date_send}</p> */}
        </li>
);
export default Message;
