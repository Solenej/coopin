import React from 'react';
import { Route, Redirect } from 'react-router-dom';

export const PrivateRouteAdmin = ({ component: Component, ...rest }) => (
  <Route {...rest} render={props => {
    return (
      JSON.parse(localStorage.getItem('user')).roles[0] === "ROLE_ADMIN"
          ? <Component {...props} />
          : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
  )}} />

)


