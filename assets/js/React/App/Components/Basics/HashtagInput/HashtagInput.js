import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import InputAdornment from '@material-ui/core/InputAdornment';

const styles = {
    purple: {
            color: '#7b2292'
    },
    orange: {
        color: '#EF7400'    
    }
}

class HashtagInput extends React.Component {

    constructor(props, context) {
        super(props, context);

    }

    render() {

        const { classes, index, onChange } = this.props;
        return <section className="hashtag-input">
            <Input
            onChange={onChange}
                className={classes.purple + " " + (index % 2 === 0 ? classes.orange : "")}
                startAdornment={<InputAdornment position="start">#</InputAdornment>}
            ></Input>
        </section>
    }
}

export default withStyles(styles)(HashtagInput);