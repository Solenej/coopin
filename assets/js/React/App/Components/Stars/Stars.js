import React from 'react';
import {Link} from 'react-router-dom';
import {
    Typography,
    withStyles,
    Button,
    Icon
} from '@material-ui/core';

class Stars extends React.Component {
    constructor(props, context) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
        this.state = {
            star1: false,
            star2: false,
            star3: false,
            star4: false,
            star5: false,
            notation: null    /* A envoyer au Back*/
        }
    }

    handleClick(star) {
        const{
            star1,
            star2,
            star3,
            star4
        } = this. state;
        if (star === 'star1') {
            if (star1 === true && star2 === true) {
                this.setState({
                    star2: false,
                    star3: false,
                    star4: false,
                    star5: false,
                    notation: 1
                })
            } else if (star1 === false) {
                this.setState({
                    star1: true,
                    notation: 1
                })
            } else if (star1 === true && star2 === false) {
                this.setState({
                    star1: false,
                    notation: null
                })
            }
        } else if (star === 'star2') {
            if (star2 === true) {
                this.setState({
                    star1: true,
                    star3: false,
                    star4: false,
                    star5: false,
                    notation: 2
                })
            }
            else if (star2 === false ) {
                this.setState({
                    star1: true,
                    star2: true,
                    notation: 2
                })
            }
        } else if (star === 'star3') {
            if (star3 === true) {
                this.setState({
                    star1: true,
                    star2: true,
                    star4: false,
                    star5: false,
                    notation: 3
                })
            }
            else if (star3 === false) {
                this.setState({
                    star1: true,
                    star2: true,
                    star3: true,
                    notation: 3
                })
            }
        } else if (star === 'star4') {
            if (star4 === true) {
                this.setState({
                    star1: true,
                    star2: true,
                    star3: true,
                    star5: false,
                    notation: 4
                })
            }
            else if (star4 === false){
                this.setState({
                    star1: true,
                    star2: true,
                    star3: true,
                    star4: true,
                    notation: 4
                })
            }
        } else if (star === 'star5') {
            this.setState({
                star1: true,
                star2: true,
                star3: true,
                star4: true,
                star5: true,
                notation: 5
            }) 
        }
    };

    render() {
        const {
            classes,
            handleSubmitStars,
            handleAddStars
        } = this.props;
        const {
            star1,
            star2,
            star3,
            star4,
            star5,
            notation
        } = this.state;
        return (
            <div className="stars-containers">
                <Typography className={classes.typoStars} variant="body1">Utiliser notre <span className="orange">APP</span>, c'était comment ?</Typography>
                <div className="stars-block">
                    <Button onClick={() => { this.handleClick("star1") }}><Icon className={classes.buttonStarHover} fontSize="large" color={star1 ? "secondary" : "disabled"}>stars</Icon></Button>
                    <Button onClick={() => { this.handleClick("star2") }}><Icon className={classes.buttonStarHover} fontSize="large" color={star2 ? "secondary" : "disabled"}>stars</Icon></Button>
                    <Button onClick={() => { this.handleClick("star3") }}><Icon className={classes.buttonStarHover} fontSize="large" color={star3 ? "secondary" : "disabled"}>stars</Icon></Button>
                    <Button onClick={() => { this.handleClick("star4") }}><Icon className={classes.buttonStarHover} fontSize="large" color={star4 ? "secondary" : "disabled"}>stars</Icon></Button>
                    <Button onClick={() => { this.handleClick("star5") }}><Icon className={classes.buttonStarHover} fontSize="large" color={star5 ? "secondary" : "disabled"}>stars</Icon></Button>
                </div>
                <Button
                    className={classes.buttonSubmit}
                    size="large"
                    variant="contained"
                    color="primary"
                    disabled={notation === null}
                    onClick={async () => {
                        await handleAddStars(notation)
                        await handleSubmitStars()}
                    }
                >
                Valider
                </Button>
            </div>
        )
    };
}

const styleStars = () => ({
    typoStars: {
        fontWeight: 550,
        margin: '20px auto'
    },
    buttonStarHover: {
        '&:hover': {
            color: '#EF7400',
            cursor: 'pointer'
        }
    },
    buttonSubmit: {
        color: 'white',
        margin: 20,
        textDecoration: 'none'
    }
})
export default withStyles(styleStars)(Stars);