import React from 'react';
import ReactDOM from 'react-dom';
import Message from '../Basics/Message/Message';
import ButtonChatRoom from '../Button/Button'
import { withStyles, Typography } from '@material-ui/core';
import PropTypes from 'prop-types';
import Exit from '../../Containers/Evaluation/Exit/Exit';

class ChatRoom extends React.Component {
    constructor(props) {
        super(props);

        this.submitMessage = this.submitMessage.bind(this);
        this.onEnterPress = this.onEnterPress.bind(this);

    }

    async componentDidMount() {
        const {
            connexionChat,
            deconnexionChat,
            statutConnexion,
            loadData
        } = this.props;
        await loadData();
        const {
            session_id,
        } = this.props;

        if(!statutConnexion) {
            this.conn = new WebSocket(`${this.props.basename}/chatSession/${session_id}`);
            this.conn.onmessage = this.onMessage.bind(this);
            this.conn.onopen = () => {
                console.log('connexion ouverte');
                connexionChat();
            }
            this.conn.onclose = () => {
                console.log('connexion fermée');
                deconnexionChat();
            }
        };

            this.scrollToBot();
    }

    componentDidUpdate() {
        this.scrollToBot();
    }

    scrollToBot() {
        const {
            event,
            eventOther
        } = this.props;
        if(event !== 'PAUSE' && event !== 'STOP' && eventOther !== 'STOP' && eventOther !== 'PAUSE') {
            ReactDOM.findDOMNode(this.refs.chats).scrollTop = ReactDOM.findDOMNode(this.refs.chats).scrollHeight;
        }
    }

    onMessage(data) {
        const {
            session_id,
            chatSessionUserId,
            addMessage,
            addEventOther
        } = this.props;

        const d = JSON.parse(data.data);

        const chat = {
            session_id,
            chatSessionUserId,
            username: d.username,
            texte: d.message
        };
        addMessage(chat);
        addEventOther(d.event)
    }

    onEnterPress(e) {
        if(e.keyCode == 13 && e.shiftKey == false) {
          e.preventDefault();
          this.submitMessage('MESSAGE', e);
        }
      }

    componentWillUnmount() {
        const {
            deconnexionChat
        } = this.props;
        if (this.conn) {
            this.conn.close();
        }
        deconnexionChat();
    }
    submitMessage(eventType, e) {
        const {
            jwt_token,
            username,
            session_id,
            chatSessionUserId,
            addMessage,
            addEvent
        } = this.props;

        switch(eventType) {
            case 'MESSAGE': {
                e.preventDefault();
                const message = this.refs.msg.value.trim();
                if(message !== "") {
                    this.conn.send(JSON.stringify({
                        token: jwt_token,
                        username,
                        event: 'MESSAGE',
                        message
                    }))

                    const chat = {
                        session_id,
                        chatSessionUserId,
                        username,
                        texte: message
                    };
                    addMessage(chat);
                    addEvent('MESSAGE')
                    ReactDOM.findDOMNode(this.refs.msg).value = "";
                }
                break;
            }
            case 'PAUSE': {
                this.conn.send(JSON.stringify({
                    token: jwt_token,
                    username,
                    event: 'PAUSE'
                }))
                addEvent('PAUSE')
                break;
            }
            case 'PLAY': {
                this.conn.send(JSON.stringify({
                    token: jwt_token,
                    username,
                    event: 'RESUME'
                }))
                addEvent('RESUME')
                break;
            }
            default:
                return
        }
    }
    render() {
        const {
            classes,
            username,
            chats,
            event,
            eventOther,
            startChat
         } = this.props;
        return (
            <div className="chatRoom-container">
                {(eventOther !== "PAUSE" && event !== "PAUSE" && eventOther !== "STOP") &&
                    <div className="chatRoom-container">
                        <ButtonChatRoom
                            pause
                            onClickPause={(e) => this.submitMessage('PAUSE', e)}
                            onClickStop={(e) => this.submitMessage('STOP', e)}
                            classNamePause={classes.pauseButton}
                            classNameStop={classes.stopButton}
                        />
                        <div className="chatroom"  ref="chats">

                            <ul className="chats">
                                {
                                    chats.map((chat, key) =>
                                        <Message chat={chat} user={username} key={key}/>
                                    )
                                }
                            </ul>
                        </div>
                        <form className="input" id="form" onSubmit={(e) => this.submitMessage('MESSAGE', e)}>
                            <textarea autoFocus type="text" rows="4" cols="30" ref="msg" placeholder="Ecrivez votre message" id="messageTextarea" onKeyDown={(e) => this.onEnterPress(e)}/>
                            <input type="submit" value=""/>
                        </form>
                    </div>
                }
                {((event === "PAUSE" && eventOther !== 'STOP') || startChat) &&
                    <div className="break-img">
                        <ButtonChatRoom
                            play
                            classNamePause={classes.playButton}
                            classNameStop={classes.stopButton1}
                            onClickPlay={(e) => this.submitMessage('PLAY', e)}
                            onClickStop={(e) => this.submitMessage('STOP', e)}
                        />
                        <img className="break-img-1" src={require('../../../../../images/chouette_en_standby-removebg.png')}></img>
                        <img className="break-img-2" src={require('../../../../../images/speech-bubbles.png')}></img>
                    </div>
                }
                {(eventOther === "PAUSE" && event !== "PAUSE") &&
                    <div className="break-img">
                        <Typography
                            className={classes.title1}
                            variant="h2"
                            color="primary"
                        >
                            Ta copine fait Pause.
                        </Typography>
                        <ButtonChatRoom
                        not
                        stop
                        classNameStop={classes.stopButtonNot}
                        onClickStop={(e) => this.submitMessage('STOP', e)}
                        />
                        <img className="break-img-1" src={require('../../../../../images/chouette_en_standby-removebg.png')}></img>
                        <img className="break-img-2" src={require('../../../../../images/speech-bubbles.png')}></img>
                    </div>
                }
                {eventOther === "STOP" &&
                            <Exit quit/>
                        }
            </div>
        )
    }
}

const styles = theme => ({
    '@global': {
      '*::-webkit-scrollbar': {
        width: '0.4em'
      },
      '*::-webkit-scrollbar-track': {
        '-webkit-box-shadow': 'inset 0 0 6px rgba(0,0,0,0.00)'
      },
      '*::-webkit-scrollbar-thumb': {
        backgroundColor: 'rgba(0,0,0,.1)',
        outline: '1px solid slategrey'
      }
    }
});

ChatRoom.propType = {
classes: PropTypes.shape({}).isRequired,
session_id: PropTypes.number.isRequired,
chats: PropTypes.array,
startChat: PropTypes.bool
}

ChatRoom.defaultProps = {
chats: [],
startChat: false
};
export default withStyles(styles)(ChatRoom);
