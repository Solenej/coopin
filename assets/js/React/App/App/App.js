import React from "react";
import MuiThemeProvider from "@material-ui/core/styles/MuiThemeProvider";
import theme from "../../theme/index";
import { Router, Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';

import { history } from '../Helpers';
import { alertActions } from '../Redux/actions';
import { PrivateRoute } from '../Components/Basics/PrivateRoute/PrivateRoute';
import { PrivateRouteAdmin } from '../Components/Basics/PrivateRoute/PrivateRouteAdmin';

import Header from "../Components/Header/Header";
import Home from "../Components/Home/Home";
import Match from "../Containers/Match/Match";
import Search from "../Containers/Search/Search";
import End from "../Components/End/End";
import ChatRoom from "../Containers/chatRoom/chatRoom";
import Break from "../Containers/Break/Break";
import Access from "../Containers/Access/Access";
import Stars from "../Containers/Evaluation/Stars/Stars";
import Admin from "../Containers/Admin/Admin";
import Exit from "../Containers/Evaluation/Exit/Exit";
import EvaluationFeelings from "../Containers/Evaluation/Feelings/Feelings";
import EvaluationHashtags from "../Containers/Evaluation/Hashtags/Hashtags";

class App extends React.Component {

  // constructor(props) {
  //   super(props);

  //   const { dispatch } = this.props;
  //   history.listen((location, action) => {
  //       // clear alert on location change
  //       dispatch(alertActions.clear());
  //   });
  // }

  render() {
    // const { alert } = this.props;
    // console.log('app',{alert});

    return (
        <MuiThemeProvider theme={theme}>
          {/* {alert.message &&
            <div className={`alert ${alert.type}`}>{alert.message}</div>
          } */}
          <Router history={history}>
            <Header />
            <Switch>
              <Route
                path={"/login"}
                component={Access}
                props={alert}
              />
              <Route exact
                path={"/"}
                component={Home}
              />
              <PrivateRoute
                path={"/match"}
                component={Match}
              />
              <PrivateRoute
                path={"/end"}
                component={End}
              />
              <PrivateRoute
                path={"/search"}
                component={Search}
                basename={process.env.WS_BASE}
                />
              <PrivateRoute
                path={"/chatroom"}
                component={ChatRoom}
                basename={process.env.WS_BASE}
                />
              <PrivateRoute
                path={"/break"}
                component={Break}
                />
              <PrivateRoute
                path={"/stars"}
                component={Stars}
                />
              <PrivateRouteAdmin
                path={"/admin"}
                component={Admin}
                />
              <PrivateRoute
                path={"/evaluation-hashtags"}
                component={EvaluationHashtags}
                />
              <PrivateRoute
                path={"/evaluation-feelings"}
                component={EvaluationFeelings}
                />
              <PrivateRoute
                path={"/exit"}
                component={Exit}
                basename={process.env.WS_BASE}
              />
            </Switch>
          </Router>
        </MuiThemeProvider>
    );
  }
}

function mapStateToProps(state) {
  const { alert } = state;
  return {
      alert
  };
}

export default connect(mapStateToProps)(App);
