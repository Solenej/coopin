import { evaluationActions } from '../Redux/actions';
import { authHeader, history, authParams } from '../Helpers';
import _ from 'lodash';

export const getThemes = () => async (dispatch) => {
  const header = authHeader();
  header['Content-Type'] =  'application/json';
    const requestOptions = {
      method: 'GET',
      headers: header
    };
    const param = `?access_token=${authParams()}`
  try {
    await dispatch(evaluationActions.getThemesRequest());
    const data = await fetch(`/api/listThemes${param}`, requestOptions).then(handleResponse);
    await dispatch(evaluationActions.getThemesSuccess(data));
    return data;
  } catch (err) {
    await dispatch(evaluationActions.getThemesFail());
    console.log({ err });
    await dispatch(handleResponse(err));
    return err;
  }
}

export const postEval = () => async (dispatch, getState) => {
  const state = getState();
  const comment = _.get(state, 'evaluation.comment', "");
  const hashtags = _.get(state, 'evaluation.hashtags', {});
  const otherHashtags = _.get(state, 'evaluation.otherHashtags', []);
  const feeling = _.get(state, 'evaluation.feelings', null);
  const stars = _.get(state, 'evaluation.stars', null);
  const sessionId = _.get(state, 'search.sessionId', 'match.sessionId')
  const chatSessionUser = _.get(state, 'search.chatSessionUserId', 'match.chatSessionUserId')
  const evaluation = {
    comment,
    hashtags,
    otherHashtags,
    feeling,
    stars,
    sessionId,
    chatSessionUser
  };
  const header = authHeader();
  header['Content-Type'] =  'application/json';
    const requestOptions = {
      method: 'POST',
      headers: header,
      body: JSON.stringify( {evaluation} )
    };
    const param = `?access_token=${authParams()}`
    try {
      await dispatch(evaluationActions.postEvaluationRequest());
      const data = await fetch(`/api/newEvaluation${param}`, requestOptions);
      await dispatch(evaluationActions.postEvaluationSuccess(data));
      history.push('/end');
      return data;
    } catch (err) {
      await dispatch(evaluationActions.postEvaluationFail());
      console.log({ err });
      await dispatch(handleResponse(err));
      return err;
    }
  }

  function handleResponse(response) {
    return response.text().then(text => {
      const data = text && JSON.parse(text);
      if (!response.ok) {

        const error = (data && data.message) || response.statusText;
        return Promise.reject(error);
      }

      return data;
    });
  }
