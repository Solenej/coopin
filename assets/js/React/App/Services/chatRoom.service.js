import actions from '../Redux/actions/chatRoom.actions';
import { authHeader, authParams } from '../Helpers';
import _ from 'lodash';

export const getChat = () => async(dispatch, getState) => {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };
    const param = `?access_token=${authParams()}`
    const state = getState();
    const sessionId = _.get(state, 'search.sessionId','match.sessionId');
    const chatSessionUserId = _.get(state, 'search.chatSessionUserId', 'match.chatSessionUserId')
    try {
        await dispatch(actions.getMessageRequest());
        const data = await fetch(`/api/chatSession/${sessionId}/messages${param}`, requestOptions).then(handleResponse);
        const chat = data.map(e => ({
          sessionId,
          chatSessionUserId,
          username: e.username,
          texte: e.texte
        }));
        await dispatch(actions.getMessageSuccess(chat, sessionId));
        return data
    } catch (err) {
        await dispatch(actions.getMessageFailure());
        console.log(err);
        await dispatch(handleResponse(err));
        return err;
    }
}

function handleResponse(response) {
    return response.text().then(text => {
      const data = text && JSON.parse(text);
      if (!response.ok) {
        const error = (data && data.message) || response.statusText;
        return Promise.reject(error);
      }
      return data;
    });
  }
  