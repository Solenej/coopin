
import actions from '../Redux/actions/admin.actions';
import { getThemes } from '../Services/index';
import { authHeader, history, authParams } from '../Helpers';

export const getUsersList = () => async dispatch => {
  const requestOptions = {
    method: 'GET',
    headers: authHeader()
  };
  const param = `?access_token=${authParams()}`;
  try {
    await dispatch(actions.getListUsersRequest());
    const data = await fetch(`/api/listUsers${param}`, requestOptions).then(handleResponse);
    if(data.ok) {
      await dispatch(actions.getListUsersSuccess(data.ok));
    }
    return data;
  } catch (err) {
    await dispatch(actions.getListUsersFailure());
    console.log({err});
    await dispatch(handleResponse(err));
    return err;
  }
}
export const getUserGroupList = () => async dispatch => {
  const requestOptions = {
    method: 'GET',
    headers: authHeader()
  };
  const param = `?access_token=${authParams()}`;
  try {
    await dispatch(actions.getListUserGroupRequest());
    const data = await fetch(`/api/listUsersGroups${param}`, requestOptions).then(handleResponse);
    if(data.ok) {
      await dispatch(actions.getListUserGroupSuccess(data.ok));
    }
    return data;
  } catch (err) {
    await dispatch(actions.getListUserGroupFail());
    console.log({err});
    return err;
  }
}
export const getGroupList = () => async (dispatch) => {
  const requestOptions = {
    method: 'GET',
    headers: authHeader()
  };
  const param = `?access_token=${authParams()}`;
  try {
    await dispatch(actions.getListGroupsRequest());
    const data = await fetch(`/api/listGroups${param}`, requestOptions).then(handleResponse);
    await dispatch(actions.getListGroupsSuccess(data));
    return data;
  } catch (err) {
    await dispatch(actions.getListGroupsFail());
    console.log({ err });
    await dispatch(handleResponse(err));
    return err;
  }
};

export const getText = () => async (dispatch) => {
  const requestOptions = {
    method: 'GET',
    headers: authHeader()
  };
  try {
    await dispatch(actions.getTextRequest());
    const data = await fetch(`/api/getExplanation`, requestOptions).then(handleResponse);
    await dispatch(actions.getTextSuccess(data));
    return data;
  } catch (err) {
    await dispatch(actions.getTextFail());
    console.log({ err });
    // await dispatch(handleResponse(err));
    return err;
  }
};

export const postGroup = _nameGroup => async dispatch => {
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ _nameGroup })
  };
  const param = `?access_token=${authParams()}`
  try {
    await dispatch(actions.postGroupRequest());
    const data = await fetch(`/api/newGroup${param}`, requestOptions).then(handleResponse);
    await dispatch(actions.postGroupSuccess(data));
    await dispatch(getGroupList());
    // history.push('/admin');
    return data;
  } catch (err) {
    await dispatch(actions.postGroupFail());
    console.log({ err });
    await dispatch(handleResponse(err));
    return err;
  }
}

export const postTheme = _nameTheme => async dispatch => {
  console.log(_nameTheme);
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ _nameTheme })
  };
  const param = `?access_token=${authParams()}`
  try {
    await dispatch(actions.postThemeRequest());
    const data = await fetch(`/api/newTheme${param}`, requestOptions).then(handleResponse);
    console.log(data);
    await dispatch(actions.postThemeSuccess(data));
    await dispatch(getThemes());
    return data;
  } catch (err) {
    await dispatch(actions.postThemeFail());
    console.log({ err });
    await dispatch(handleResponse(err));
    return err;
  }
}

export const postUser = (_nameGroup, _codeAccess, _checkAdmin) => async dispatch => {
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ _nameGroup, _codeAccess, _checkAdmin })
  };
  const param = `?access_token=${authParams()}`
  try {
    await dispatch(actions.postUserRequest());
    const data = await fetch(`/api/newUser${param}`, requestOptions).then(handleResponse);
    await dispatch(actions.postUserSuccess(data));
    await dispatch(getUserGroupList());
    // history.push('/admin');
    return data;
  } catch (err) {
    await dispatch(actions.postUserFail());
    console.log({ err });
    await dispatch(handleResponse(err));
    return err;
  }
}

export const postText = text => async dispatch => {
  const _textExplanation = text;
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ _textExplanation })
  };
  const param = `?access_token=${authParams()}`
  try {
    await dispatch(actions.postTextRequest());
    const data = await fetch(`/api/newText${param}`, requestOptions).then(handleResponse);
    await dispatch(actions.postTextSuccess(data));
    // history.push('/admin');
    return data;
  } catch (err) {
    await dispatch(actions.postTextFail());
    console.log({ err });
    await dispatch(handleResponse(err));
    return err;
  }
}

export const exportData = () => async (dispatch) => {
  const requestOptions = {
    method: 'GET',
    headers: authHeader()
  };
  const param = `?access_token=${authParams()}`
  try {
    await dispatch(actions.exportDataRequest());
    const data = await fetch(`/api/newExport${param}`, requestOptions).then(handleResponse);
    await dispatch(actions.exportDataSuccess(data));
  } catch (err) {
    await dispatch(actions.exportDataFail());
    console.log({ err });
    await dispatch(handleResponse(err));
    return err;
  }
}

function handleResponse(response) {
  return response.text().then(text => {
    const data = text && JSON.parse(text);
    if (!response.ok) {
      const error = (data && data.message) || response.statusText;
      return Promise.reject(error);
    }
    return data;
  });
}
