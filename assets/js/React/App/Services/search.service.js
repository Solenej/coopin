import { searchActions } from '../Redux/actions/search.actions';
import { authHeader, history, authParams } from '../Helpers';

export const getSession = () => async (dispatch) => {
  const header = authHeader();
  const param = `?access_token=${authParams()}`
  const requestOptions = {
    method: 'GET',
    hearders: header
  };
  try {
    await dispatch(searchActions.getSessionRequest());
    const data = await fetch(`/api/chatSession${param}`, requestOptions).then(handleResponse);
    if (data) {
    await dispatch(searchActions.getSessionSuccess(data));
      switch(data.state) {
        case "STARTED":
        case "RESUMED":
          history.push('/chatRoom');
          break;
        case "STOPPED":
          history.push({
            pathname: '/exit',
            state: {startChat: true}
          });
          break;   
        case "PAUSED":
          history.push({
            pathname: '/chatRoom',
            state: { startChat: true }
          });
          break;
        default:
          return;
      }
    }
    return data;
  } catch (err) {
    await dispatch(searchActions.getSessionFailure());
    return err;
  }
};


function handleResponse(response) {
  return response.text().then(text => {
    const data = text && JSON.parse(text);
    if (!response.ok) {
      const error = (data && data.message) || response.statusText;
      return Promise.reject(error);
    }
    return data;
  });
}

