import { connect } from 'react-redux';
import { userActions } from '../../Redux/actions';
import Access from '../../Components/Access/Access';
import {
    getText
  } from '../../Services/index';

const mapStateToProps = state => ({
    errorType: _.get(state, 'alert.type', ''),
    loggingIn: _.get(state, 'authentication.loggingIn', false),
    textExplication: _.get(state, 'admin.textExplication.value', '')
});

const mapDispatchToProps = dispatch => ({
    getTexExplicatif: () => dispatch(getText()),
    logout: () => dispatch(userActions.logout()),
    login: (username, password) =>dispatch(userActions.login(username, password))
})

const mergeProps = (stateProps, dispatchProps, ownProps) => {
    const {
        errorType,
        loggedIn,
        loggingIn,
        textExplication
    } = stateProps;
    const {
        getTexExplicatif,
        logout,
        login
    } = dispatchProps;


    const loadData = async () => {
      try {
        await logout();
        await getTexExplicatif();
      } catch (err) {
        throw err;
      }
    };

    const logIn = async (username, password) => {
        try {
            await login(username, password);
        } catch (err) {
            throw err;
        }
    };

    return {
      ...stateProps,
      ...dispatchProps,
      ...ownProps,
      loadData,
      errorType,
      loggedIn,
      logIn,
      loggingIn,
      textExplication
    };
  };

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(Access);
