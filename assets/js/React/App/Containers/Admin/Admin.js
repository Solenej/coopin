import { connect } from 'react-redux';
import Admin from '../../Components/Admin/Admin';
import _ from 'lodash';
import {
  getGroupList,
  postGroup,
  getText,
  postText,
  postTheme,
  postUser,
  getThemes,
  exportData,
  getUsersList,
  getUserGroupList
} from '../../Services/index';

const mapStateToProps = state => ({
  listGroup: _.get(state, 'admin.listGroup.ok', []),
  listTheme: _.get(state, 'evaluation.themes.ok', []),
  errorGroup: _.get(state, 'admin.errorGroup.error', ''),
  textExplication: _.get(state, 'admin.textExplication.value', ''),
  errorText: _.get(state, 'admin.errorText.error', ''),
  errorUser: _.get(state, 'admin.errorUser.error', ""),
  errorTheme: _.get(state, 'admin.errorTheme.error', ""),
  errorData: _.get(state, 'admin.errorData'),
  listUsers: _.get(state, 'admin.listUsers', []),
  listUserGroup: _.get(state, 'admin.listUserGroup', [])
});

const mapDispatchToProps = dispatch => ({
getGroupList: () => dispatch(getGroupList()),
getTexExplicatif: () => dispatch(getText()),
createGroup: nameGroup => dispatch(postGroup(nameGroup)),
createUser: ( nameGroup, nameUser, checkUser ) => dispatch(postUser(nameGroup, nameUser, checkUser)),
createText: text => dispatch(postText(text)),
createTheme: theme => dispatch(postTheme(theme)),
exportData: () => dispatch(exportData()),
getThemes: () => dispatch(getThemes()),
getUsersList: () => dispatch(getUsersList()),
getUserGroupList: () => dispatch(getUserGroupList())
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    listGroup,
    listTheme,
    textExplication,
    errorGroup,
    errorText,
    errorUser,
    errorTheme,
    errorData,
    listUsers,
    listUserGroup
  } = stateProps;
  const {
    getGroupList,
    getThemes,
    getTexExplicatif,
    createGroup,
    createUser,
    createText,
    createTheme,
    exportData,
    getUserGroupList
  } = dispatchProps;

  const loadData = async () => {
    try {
      await getThemes();
      await getGroupList();
      await getTexExplicatif();
      await getUserGroupList();
    } catch (err) {
      throw err;
    }
  };

  const onSubmitGroup = (nameGroup) => {
    createGroup(nameGroup);
  };

  const onSubmitUser = (nameGroup, nameUser, check) => {
    createUser(nameGroup, nameUser, check);
  };

  const onSubmitText = (text) => {
    createText(text);
  }

  const onSubmitTheme = (theme) => {
    createTheme(theme);
  }


  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    listGroup,
    listTheme,
    listUsers,
    loadData,
    onSubmitUser,
    onSubmitGroup,
    onSubmitText,
    onSubmitTheme,
    textExplication,
    exportData,
    errorGroup,
    errorText,
    errorUser,
    errorTheme,
    errorData,
    listUserGroup
  };
};
export default connect(mapStateToProps,mapDispatchToProps,mergeProps)(Admin);


