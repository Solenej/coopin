import { connect } from 'react-redux';
import chatRoom from '../../Components/ChatRoom/ChatRoom';
import actions from '../../Redux/actions/chatRoom.actions';
import {
  getSession,
  getChat
} from '../../Services/index';


const mapStateToProps = state => ({
  session_id: _.get(state, 'search.sessionId', 'match.sessionId'),
  chatSessionUserId: _.get(state, 'search.chatSessionUserId', 'match.chatSessionUserId'),
  jwt_token: _.get(state, 'authentication.user.token', ''),
  username: _.get(state, 'authentication.user.username', ''),
  chatsStore: _.get(state, 'chatRoom.chats', {}),
  event: _.get(state, 'chatRoom.event', ''),
  eventOther: _.get(state, 'chatRoom.eventOther', ''),
  statutConnexion: _.get(state, 'chatRoom.connexionChatRoom', false)
});

const mapDispatchToProps = dispatch => ({
addMessage: chat => dispatch(actions.addMessage(chat)),
addEvent: event => dispatch(actions.addEvent(event)),
addEventOther: event => dispatch(actions.addEventOther(event)),
connexionChat: () => dispatch(actions.connexionChatRoom()),
deconnexionChat: () => dispatch(actions.deconnexionChatRoom()),
getSessionId: () => dispatch(getSession()),
getChat: () => dispatch(getChat())
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    session_id,
    jwt_token,
    username,
    chatSessionUserId,
    chatsStore,
    event,
    eventOther,
    statutConnexion
  } = stateProps;
  const {
    addMessage,
    addEvent,
    addEventOther,
    connexionChat,
    deconnexionChat,
    getSessionId,
    getChat
  } = dispatchProps;

  const loadData = async() => {
    try {
      if(session_id === null) {
        await getSessionId();
      }
        await getChat();
    } catch (err) {
      throw err;
    }
  }
  const chats = _.get(chatsStore, `${session_id}`, [])
  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    session_id,
    jwt_token,
    username,
    chatSessionUserId,
    addMessage,
    addEventOther,
    chats,
    event,
    addEvent,
    eventOther,
    connexionChat,
    deconnexionChat,
    statutConnexion,
    loadData,
    getSessionId
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(chatRoom);
