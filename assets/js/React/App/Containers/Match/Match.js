import { connect } from 'react-redux';
import Match from '../../Components/Match/Match';
import _ from 'lodash';

const mapStateToProps = state => ({
  matched: _.get(state, 'match.matched', false)
});


export default connect(mapStateToProps)(Match);
