import { connect } from 'react-redux';
import Search from '../../Components/Search/Search';
import _ from 'lodash';
import actions from '../../Redux/actions/chatRoom.actions';
import { getSession } from '../../Services/index';
import { matchActions } from '../../Redux/actions/match.actions';

const mapStateToProps = state => ({
  matchOver: _.get(state, 'match.matchOver', false),
  username: _.get(state, 'authentication.user.username', ''),
  jwt_token: _.get(state, 'authentication.user.token', ''),
  session_id: _.get(state, 'search.sessionId', 'match.sessionId')
});

const mapDispatchToProps = dispatch => ({
  addStatusMatch: status => dispatch(matchActions.addStatusMatch(status)),
  deconnexionChat: () => dispatch(actions.deconnexionChatRoom()),
  getSessionId: () => dispatch(getSession())
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    matchOver,
    username,
    jwt_token,
    session_id
  } = stateProps;
  const {
    addStatusMatch,
    deconnexionChat,
    getSessionId
  } = dispatchProps;

  const loadData = async () => {
    try {
      await getSessionId();
    } catch (err) {
      throw err;
    }
  };
  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    matchOver,
    jwt_token,
    username,
    session_id,
    deconnexionChat,
    addStatusMatch,
    loadData
  };
};
export default connect(mapStateToProps,mapDispatchToProps,mergeProps)(Search);


