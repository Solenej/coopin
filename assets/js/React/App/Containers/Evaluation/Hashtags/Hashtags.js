import { connect } from 'react-redux';
import Hashtags from '../../../Components/EvaluationHashtags/EvaluationHashtags';
import _ from 'lodash';
import {
    evaluationActions
} from '../../../Redux/actions';

const mapStateToProps = state => ({
    themes: _.get(state, 'evaluation.themes.ok', [])
});

const mapDispatchToProps = dispatch => ({
    addHashtags: hashtags => dispatch(evaluationActions.addHashtags(hashtags)),
    addOtherHashtags: otherHashtags => dispatch(evaluationActions.addOtherHashtags(otherHashtags)),
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
    const {
        addHashtags,
        addOtherHashtags
    } = dispatchProps;

    const { 
        themes
    } = stateProps;
    
    const handleSubmit = (a, b) => {
        try {
            addHashtags(a);
            addOtherHashtags(b);
        } catch (err) {
            console.log({err});
            throw err;
        }
    };

    let hashtagsObject = {};    

    return {
        ...stateProps,
        ...dispatchProps,
        ...ownProps,
        handleSubmit,
        hashtagsObject,
        themes
    };

};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(Hashtags);