import { connect } from 'react-redux';
import Exit from '../../../Components/Exit/Exit';
import {
    evaluationActions,
} from '../../../Redux/actions';
import actions from '../../../Redux/actions/chatRoom.actions';
import {
    getThemes
  } from '../../../Services/index';



const mapStateToProps = state=> ({
    session_id: _.get(state, 'search.sessionId', 'match.sessionId'),
    eventOther: _.get(state, 'chatRoom.eventOther', ''),
    jwt_token: _.get(state, 'authentication.user.token', ''),
    username: _.get(state, 'authentication.user.username', ''),
});

const mapDispatchToProps = dispatch => ({
    addComment: comment => dispatch(evaluationActions.addComment(comment)),
    addEvent: event => dispatch(actions.addEvent(event)),
    deconnexionChat: () => dispatch(actions.deconnexionChatRoom()),
    addMessage: chat => dispatch(actions.addMessage(chat)),
    addEventOther: event => dispatch(actions.addEventOther(event)),
    getThemes: () => dispatch(getThemes())
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
    const {
        session_id,
        eventOther,
        jwt_token,
        username
    } = stateProps;
    const {
        addComment,
        addEvent,
        deconnexionChat,
        addMessage,
        addEventOther,
        getThemes
    } = dispatchProps;

    const loadThemes = async () => {
        try{
            await getThemes();
        } catch (err){
            console.err({err});
            throw err;
        }
    }

    return {
        ...stateProps,
        ...dispatchProps,
        ...ownProps,
        addComment,
        session_id,
        eventOther,
        addEvent,
        jwt_token,
        username,
        addMessage,
        deconnexionChat,
        addEventOther,
        loadThemes
    };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(Exit);
