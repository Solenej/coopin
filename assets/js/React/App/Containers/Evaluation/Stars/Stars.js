import  { connect } from 'react-redux';
import Stars from '../../../Components/Stars/Stars';
import {
    evaluationActions
} from '../../../Redux/actions';
import {
    postEval
} from '../../../Services';


const mapStateToProps = () => ({
});

const mapDispatchToProps = dispatch => ({
    addStars: star => dispatch(evaluationActions.addStars(star)),
    postEval: () => dispatch(postEval())
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
    const {
        addStars,
        postEval
    } = dispatchProps;

    const handleAddStars = (star) => {
        try {
            addStars(star);
        } catch (err) {
            console.log({err});
            throw err;
        }
    };

    
    const handleSubmitStars = () => {
        try {
            postEval();
        } catch (err) {
            console.log({err});
            throw err;
        };
    };

    return {
        ...stateProps,
        ...dispatchProps,
        ...ownProps,
        handleSubmitStars,
        handleAddStars
    };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(Stars)