import { connect } from 'react-redux';
import Feelings from '../../../Components/EvaluationFeelings/EvaluationFeelings';
import {
    evaluationActions
} from '../../../Redux/actions';

const mapStateToProps = () => ({
});

const mapDispatchToProps = dispatch => ({
    addFeelings: feeling => dispatch(evaluationActions.addFeelings(feeling))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
    const {
        addFeelings
    } = dispatchProps;

    const handleSubmitFeeling = (a) => {
        try  {
            addFeelings(a);
        } catch (err) {
            console.log(err);
            throw err;
        }
    };

    return {
        ...stateProps,
        ...dispatchProps,
        ...ownProps,
        handleSubmitFeeling
    };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(Feelings);