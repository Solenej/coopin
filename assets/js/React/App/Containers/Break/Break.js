import { connect } from 'react-redux';
import Break from '../../Components/Break/Break';



const mapStateToProps = state => ({
  session_id: _.get(state, 'match.sessionId', null),
  jwt_token: _.get(state, 'authentication.user.token', ''),
  username: _.get(state, 'authentication.username', '')
});

const mapDispatchToProps = () => ({

});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    session_id,
    jwt_token,
    username
  } = stateProps;

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    session_id,
    jwt_token,
    username
  };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(Break);
