import { searchConstants } from '../constants';

const getSessionFailure = () => ({
    type: searchConstants.GET_SESSION_FAILURE
});

const getSessionRequest = () => ({
    type: searchConstants.GET_SESSION_REQUEST
});

const getSessionSuccess = session => ({
    type: searchConstants.GET_SESSION_SUCCESS,
    session
});


export const searchActions = {
    getSessionFailure,
    getSessionRequest,
    getSessionSuccess
};
