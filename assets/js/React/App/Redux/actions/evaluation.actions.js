import { evaluationConstants } from '../constants';

const postEvaluationFail = () => ({
    type: evaluationConstants.POST_EVALUATION_FAILURE
});
const postEvaluationRequest = () => ({
    type: evaluationConstants.POST_EVALUATION_REQUEST
});
const postEvaluationSuccess = () => ({
    type: evaluationConstants.POST_EVALUATION_SUCCESS
});

const getThemesFail = () => ({
    type: evaluationConstants.GET_THEMES_FAILURE
});
const getThemesRequest = () => ({
    type: evaluationConstants.GET_THEMES_REQUEST
});
const getThemesSuccess = themes => ({
    type: evaluationConstants.GET_THEMES_SUCCESS,
    themes
});

const addComment = comment => ({
    type: evaluationConstants.ADD_COMMENT,
    comment
});
const addHashtags = hashtags => ({
    type: evaluationConstants.ADD_HASHTAGS,
    hashtags
});
const addOtherHashtags = otherHashtags => ({
    type: evaluationConstants.ADD_OTHERHASHTAGS,
    otherHashtags
});
const addFeelings = feelings => ({
    type: evaluationConstants.ADD_FEELINGS,
    feelings
});
const addStars = stars => ({
    type: evaluationConstants.ADD_STARS,
    stars
});

export const evaluationActions = {
    postEvaluationFail,
    postEvaluationRequest,
    postEvaluationSuccess,
    getThemesFail,
    getThemesRequest,
    getThemesSuccess,
    addComment,
    addHashtags,
    addOtherHashtags,
    addFeelings,
    addStars
};