import { chatRoomConstants } from '../constants/index';

const addMessage = chat => ({
    type: chatRoomConstants.ADD_MESSAGE,
    chat
});
const addEvent = event => ({
    type: chatRoomConstants.ADD_EVENT,
    event
});
const addEventOther = event => ({
    type: chatRoomConstants.ADD_EVENT_OTHER,
    event
});

const connexionChatRoom = () => ({
    type: chatRoomConstants.CONNEXION_CHATROOM
});
const deconnexionChatRoom = () => ({
    type: chatRoomConstants.DECONNEXION_CHATROOM
});

const getMessageFailure = () => ({
    type: chatRoomConstants.GET_MESSAGE_FAILURE
});
const getMessageRequest = () => ({
    type: chatRoomConstants.GET_MESSAGE_REQUEST
});
const getMessageSuccess = (chat, sessionId) => ({
    type: chatRoomConstants.GET_MESSAGE_SUCCESS,
    chat,
    sessionId
});

export default {
    addMessage,
    addEvent,
    addEventOther,
    connexionChatRoom,
    deconnexionChatRoom,
    getMessageFailure,
    getMessageRequest,
    getMessageSuccess
};
