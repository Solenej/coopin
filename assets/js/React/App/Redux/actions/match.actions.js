import { matchConstants } from '../constants';


const addStatusMatch = status => ({
    type: matchConstants.ADD_STATUS_MATCH,
    status
});

const getSessionFailure = () => ({
    type: matchConstants.GET_SESSION_FAILURE
});

const getSessionRequest = () => ({
    type: matchConstants.GET_SESSION_REQUEST
});

const getSessionSuccess = session => ({
    type: matchConstants.GET_SESSION_SUCCESS,
    session
});


export const matchActions = {
    addStatusMatch,
    getSessionFailure,
    getSessionRequest,
    getSessionSuccess
};
