import { adminConstants } from '../constants';

const getListUserGroupFail = () => ({
  type: adminConstants.GET_LIST_USERS_GROUP_FAILURE
});
const getListUserGroupRequest = () => ({
  type: adminConstants.GET_LIST_USERS_GROUP_REQUEST
});
const getListUserGroupSuccess = list => ({
  type: adminConstants.GET_LIST_USERS_GROUP_SUCCESS,
  list
});

const getListGroupsFail = () => ({
  type: adminConstants.GET_LIST_GROUP_FAILURE
});
const getListGroupsRequest = () => ({
  type: adminConstants.GET_LIST_GROUP_REQUEST
});
const getListGroupsSuccess = payload => ({
  type: adminConstants.GET_LIST_GROUP_SUCCESS,
  payload
});

const postGroupFail = () => ({
  type: adminConstants.POST_GROUP_FAILURE
});
const postGroupRequest = () => ({
  type: adminConstants.POST_GROUP_REQUEST
});
const postGroupSuccess = nameGroup => ({
  type: adminConstants.POST_GROUP_SUCCESS,
  nameGroup
});

const postThemeFail = () => ({
  type: adminConstants.POST_THEME_FAILURE
});
const postThemeRequest = () => ({
  type: adminConstants.POST_THEME_REQUEST
});
const postThemeSuccess = theme => ({
  type: adminConstants.POST_THEME_SUCCESS,
  theme
});

const postUserFail = () => ({
  type: adminConstants.POST_USER_FAILURE
});
const postUserRequest = () => ({
  type: adminConstants.POST_USER_REQUEST
});
const postUserSuccess = payload => ({
  type: adminConstants.POST_USER_SUCCESS,
  payload
});

const postTextFail = () => ({
  type: adminConstants.POST_TEXT_FAILURE
});
const postTextRequest = () => ({
  type: adminConstants.POST_TEXT_REQUEST
});
const postTextSuccess = text => ({
  type: adminConstants.POST_TEXT_SUCCESS,
  text
});

const getTextFail = () => ({
  type: adminConstants.GET_TEXT_FAILURE
});
const getTextRequest = () => ({
  type: adminConstants.GET_TEXT_REQUEST
});
const getTextSuccess = text => ({
  type: adminConstants.GET_TEXT_SUCCESS,
  text
});

const exportDataFail = () => ({
  type: adminConstants.EXPORT_DATA_FAILURE
});
const exportDataRequest = () => ({
  type: adminConstants.EXPORT_DATA_REQUEST
});
const exportDataSuccess = data => ({
  type: adminConstants.EXPORT_DATA_SUCCESS,
  data
});

const getListUsersFailure = () => ({
  type: adminConstants.GET_LIST_USERS_FAILURE
});
const getListUsersRequest = () => ({
  type: adminConstants.GET_LIST_USERS_REQUEST
});
const getListUsersSuccess = users => ({
  type: adminConstants.GET_LIST_USERS_SUCCESS,
  users
});
export default {
  getListGroupsFail,
  getListGroupsRequest,
  getListGroupsSuccess,
  postGroupFail,
  postGroupRequest,
  postGroupSuccess,
  postThemeFail,
  postThemeRequest,
  postThemeSuccess,
  postUserFail,
  postUserRequest,
  postUserSuccess,
  postTextFail,
  postTextRequest,
  postTextSuccess,
  getTextFail,
  getTextRequest,
  getTextSuccess,
  exportDataFail,
  exportDataRequest,
  exportDataSuccess,
  getListUsersFailure,
  getListUsersRequest,
  getListUsersSuccess,
  getListUserGroupFail,
  getListUserGroupRequest,
  getListUserGroupSuccess
};
