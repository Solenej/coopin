import { adminConstants, evaluationConstants } from '../constants';

const initialState = {
  listGroup: [],
  listTheme: [],
  textExplication: [],
  listUsers: [],
  errorGroup: {},
  errorUser: {},
  errorText: {},
  errorTheme: {},
  errorData: {},
  listUserGroup: []
};

const admin = (state = initialState, action) => {
  if (action && action.type) {
    switch (action.type) {
    case adminConstants.GET_LIST_USERS_GROUP_FAILURE: {
      return {
        ...state
      }
    }
    case adminConstants.GET_LIST_USERS_GROUP_REQUEST: {
      return {
        ...state
      }
    }
    case adminConstants.GET_LIST_USERS_GROUP_SUCCESS: {
      return {
        ...state,
        listUserGroup: action.list
      }
    }
    case adminConstants.GET_LIST_USERS_FAILURE: {
      return {
        ...state,
      }
    }
    case adminConstants.GET_LIST_USERS_REQUEST: {
      return {
        ...state
      }
    }
    case adminConstants.GET_LIST_USERS_SUCCESS: {
      return {
        ...state,
        listUsers: action.users
      }
    }
    case adminConstants.GET_LIST_GROUP_SUCCESS: {
      return {
        ...state,
        listGroup: action.payload
      };
    }
    case adminConstants.GET_LIST_GROUP_REQUEST: {
      return {
        ...state
      };
    }
    case adminConstants.GET_LIST_GROUP_FAILURE: {
      return {
        ...state,
        listGroup: {}
      };
    }
    case evaluationConstants.GET_THEMES_SUCCESS: {
      return {
        ...state,
        listTheme: action.themes
      };
    }
    case evaluationConstants.GET_THEMES_REQUEST: {
      return {
        ...state
      };
    }
    case evaluationConstants.GET_THEMES_FAILURE: {
      return {
        ...state,
        listGroup: {},
        errorTheme: action.themes
      };
    }
    case adminConstants.POST_GROUP_FAILURE: {
      return {
        ...state
      }
    }
    case adminConstants.POST_GROUP_REQUEST: {
      return {
        ...state
      }
    }
    case adminConstants.POST_GROUP_SUCCESS: {
      return {
        ...state,
        errorGroup: action.nameGroup
      }
    }
    case adminConstants.POST_THEME_FAILURE: {
      return {
        ...state
      }
    }
    case adminConstants.POST_THEME_REQUEST: {
      return {
        ...state
      }
    }
    case adminConstants.POST_THEME_SUCCESS: {
      return {
        ...state,
        errorTheme: action.theme
      }
    }
    case adminConstants.POST_USER_FAILURE: {
      return {
        ...state
      }
    }
    case adminConstants.POST_USER_REQUEST: {
      return {
        ...state
      }
    }
    case adminConstants.POST_USER_SUCCESS: {
      return {
        ...state,
        errorUser: action.payload
      }
    }
    case adminConstants.POST_TEXT_FAILURE: {
      return {
        ...state
      }
    }
    case adminConstants.POST_TEXT_REQUEST: {
      return {
        ...state
      }
    }
    case adminConstants.POST_TEXT_SUCCESS: {
      return {
        ...state,
        errorText: action.text
      }
    }
    case adminConstants.GET_TEXT_FAILURE: {
      return {
        ...state
      }
    }
    case adminConstants.GET_TEXT_REQUEST: {
      return {
        ...state
      }
    }
    case adminConstants.GET_TEXT_SUCCESS: {
      return {
        ...state,
        textExplication: action.text
      }
    }
    case adminConstants.EXPORT_DATA_FAILURE: {
      return {
        ...state
      }
    }
    case adminConstants.EXPORT_DATA_REQUEST: {
      return {
        ...state
      }
    }
    case adminConstants.EXPORT_DATA_SUCCESS: {
      return {
        ...state,
        errorData: action.data
      }
    }
    default:
      return state;
    }
  }
  return state;
};

export default admin;
