import { userConstants } from '../constants';
let user = JSON.parse(localStorage.getItem('user'));
const initialState = user ? {
  loggedIn: false,
  loggingIn: false,
  user
} : {};

export function authentication(state = initialState, action) {
  switch (action.type) {
    case userConstants.LOGIN_REQUEST:
      return {
        ...state,
        loggingIn: true,
        user: action.user,
        loggedIn: false
      };
    case userConstants.LOGIN_SUCCESS:
      return {
        ...state,
        loggedIn: true,
        user: action.user,
        loggingIn: false
      };
    case userConstants.LOGIN_FAILURE:
      return {
        ...state,
        loggedIn: false,
        loggingIn: false
      };
    case userConstants.LOGOUT:
      return {
        ...state,
        user: {},
        loggedIn: false,
        loggingIn: false
      };
    default:
      return state
  }
}
