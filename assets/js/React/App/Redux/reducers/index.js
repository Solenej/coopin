import { combineReducers } from 'redux';

import { authentication } from './authentication.reducer';
import { users } from './users.reducer';
import { alert } from './alert.reducer';
import admin from './admin.reducer';
import evaluation from './evaluation.reducer';
import match from './match.reducer';
import chatRoom from './chatRoom.reducer';
import search from './search.reducer';

const appReducer = combineReducers({
  authentication,
  users,
  alert,
  admin,
  evaluation,
  match,
  chatRoom,
  search
});

const rootReducer = (state, action) => {
  if (action.type === 'USERS_LOGOUT') {
    state = undefined
  }

  return appReducer(state, action)
}

export default rootReducer;
