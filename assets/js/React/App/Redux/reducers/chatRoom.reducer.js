import { chatRoomConstants } from '../constants';
import _ from 'lodash';

const initialState = {
    chats: {},
    event: '',
    eventOther: '',
    connexionChatRoom: false
};

const chatRoom = (state = initialState, action) => {
    if(action && action.type) {
        switch (action.type) {
        case chatRoomConstants.ADD_MESSAGE: {
            if(action.chat.texte) {
                return {
                    ...state,
                    chats: {
                        ...state.chats,
                        [action.chat.session_id]: [..._.get(state, `chats.${action.chat.session_id}`, []), action.chat]
                    }
                };
            } else {
                return {
                    ...state
                }
            }
        }
        case chatRoomConstants.ADD_EVENT: {
            return {
                ...state,
                event: action.event
            }
        }
        case chatRoomConstants.ADD_EVENT_OTHER: {
            return {
                ...state,
                eventOther: action.event
            }
        }
        case chatRoomConstants.CONNEXION_CHATROOM: {
            return {
                ...state,
                connexionChatRoom: true
            }
        }
        case chatRoomConstants.DECONNEXION_CHATROOM: {
            return {
                ...state,
                connexionChatRoom: false
            }
        }
        case chatRoomConstants.GET_MESSAGE_SUCCESS: {
            return {
                ...state,
                    chats: {
                        [action.sessionId]: action.chat
                    }

            }
        }
        case chatRoomConstants.GET_MESSAGE_FAILURE: {
            return {
                ...state
            }
        }
        case chatRoomConstants.GET_MESSAGE_REQUEST: {
            return {
                ...state
            }
        }
        default:
            return state;
        }
    }
    return state;
};

export default chatRoom;
