import { searchConstants } from '../constants';

const initialState = {
  sessionId: null,
  status: '',
  chatSessionUserId: null
};

const search = (state = initialState, action) => {
  if (action && action.type) {
    switch (action.type) {
      case searchConstants.GET_SESSION_FAILURE: {
        return {
          ...state
        }
      }
      case searchConstants.GET_SESSION_REQUEST: {
        return {
          ...state
        }
      }
      case searchConstants.GET_SESSION_SUCCESS: {
        return {
          ...state,
          sessionId: action.session.sessionId,
          status: action.session.status,
          chatSessionUserId: action.session.chatSessionUserId
        }
      }
      default:
        return state;
    }
  }
  return state;
};

export default search;
