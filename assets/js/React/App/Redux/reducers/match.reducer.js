import { matchConstants } from '../constants';

const initialState = {
  status: '',
  matchOver: false,
  sessionId: null,
  chatSessionUserId: null,
  matched: false
};

const match = (state = initialState, action) => {
  if (action && action.type) {
    switch (action.type) {
    case matchConstants.ADD_STATUS_MATCH: {
      const {
        status
      } = action;
        return {
        ...state,
        status: status.event,
        matchOver: status.event !== 'SEARCH' || status.event === 'MATCHED' || (status.event !== 'SEARCH' && status.event !== 'MATCHED'),
        sessionId: status.session_id,
        chatSessionUserId: status.chatSessionUserId,
        matched: status.event === 'MATCHED'
      }
    }
    default:
      return state;
    }
  }
  return state;
};

export default match;
