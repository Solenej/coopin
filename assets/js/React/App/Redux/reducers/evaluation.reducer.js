import { evaluationConstants } from '../constants';

const initialState = {
    comment: '',
    hashtags: {},
    otherHashtags: [],
    feelings: null,
    stars: null,
    themes: []
};

const evaluation = (state = initialState, action) => {
    if (action && action.type) {
        switch (action.type) {
        case evaluationConstants.POST_EVALUATION_FAILURE: {
            return {
                ...state
            };
        }
        case evaluationConstants.POST_EVALUATION_REQUEST: {
            return {
                ...state
            };
        }
        case evaluationConstants.POST_EVALUATION_SUCCESS: {
            return {
                ...state
            };
        }
        case evaluationConstants.GET_THEMES_SUCCESS: {
            return {
                ...state,
                themes: action.themes
            };
        }
        case evaluationConstants.GET_THEMES_REQUEST: {
            return {
                ...state
            };
        }
        case evaluationConstants.GET_THEMES_FAILURE: {
            return {
                ...state
            };
        }
        case evaluationConstants.ADD_COMMENT: {
            return {
                ...state,
                comment: action.comment
            };
        }
        case evaluationConstants.ADD_FEELINGS: {
            return {
                ...state,
                feelings: action.feelings
            };
        }
        case evaluationConstants.ADD_HASHTAGS: {
            return {
                ...state,
                hashtags: action.hashtags
            };
        }
        case evaluationConstants.ADD_OTHERHASHTAGS: {
            return {
                ...state,
                otherHashtags: action.otherHashtags
            };
        }
        case evaluationConstants.ADD_STARS: {
            return {
                ...state,
                stars: action.stars
            };
        }
        default:
            return state;
        }
    }
    return state;
};

export default evaluation;