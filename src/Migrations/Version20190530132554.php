<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190530132554 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE user_theme (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_theme_evaluation (user_theme_id INT NOT NULL, evaluation_id INT NOT NULL, INDEX IDX_FDF1DBEFB98F9087 (user_theme_id), INDEX IDX_FDF1DBEF456C5646 (evaluation_id), PRIMARY KEY(user_theme_id, evaluation_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user_theme_evaluation ADD CONSTRAINT FK_FDF1DBEFB98F9087 FOREIGN KEY (user_theme_id) REFERENCES user_theme (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_theme_evaluation ADD CONSTRAINT FK_FDF1DBEF456C5646 FOREIGN KEY (evaluation_id) REFERENCES evaluation (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_theme_evaluation DROP FOREIGN KEY FK_FDF1DBEFB98F9087');
        $this->addSql('DROP TABLE user_theme');
        $this->addSql('DROP TABLE user_theme_evaluation');
    }
}
