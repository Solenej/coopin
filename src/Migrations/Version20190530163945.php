<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190530163945 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        //A réactiver lors de la mise en prod + ajouter l'admin à la main
        //$this->addSql("INSERT INTO `groupe` (`id`, `libelle`) VALUES (1, 'Groupe Admin')");
        $this->addSql("DELETE FROM theme");
        $this->addSql("INSERT INTO `theme` (`id`, `name`) VALUES 
        (1, 'sport'),
        (2, 'couple'),
        (3, 'apparence'),
        (4, 'MOI'),
        (5, 'jugement'),
        (6, 'formephysique'),
        (7, 'alimentation'),
        (8, 'bouger'),
        (9, 'viesociale'),
        (10, 'ViePro'),
        (11, 'santé'),
        (12, 'Fringues');");
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
    }
}
