<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190430091357 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE session (id INT AUTO_INCREMENT NOT NULL, status INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE chat_session_user CHANGE session id_session_id INT NOT NULL');
        $this->addSql('ALTER TABLE chat_session_user ADD CONSTRAINT FK_B509BFE2C4B56C08 FOREIGN KEY (id_session_id) REFERENCES session (id)');
        $this->addSql('CREATE INDEX IDX_B509BFE2C4B56C08 ON chat_session_user (id_session_id)');

        
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE chat_session_user DROP FOREIGN KEY FK_B509BFE2C4B56C08');
        $this->addSql('DROP TABLE session');
        $this->addSql('DROP INDEX IDX_B509BFE2C4B56C08 ON chat_session_user');
        $this->addSql('ALTER TABLE chat_session_user CHANGE id_session_id session INT NOT NULL');
    }
}
