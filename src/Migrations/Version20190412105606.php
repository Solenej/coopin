<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190412105606 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE chat_session_user (id INT AUTO_INCREMENT NOT NULL, id_user_id INT NOT NULL, session INT NOT NULL, INDEX IDX_B509BFE279F37AE5 (id_user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE evaluation (id INT AUTO_INCREMENT NOT NULL, id_chat_session_user_id INT NOT NULL, point INT NOT NULL, text LONGTEXT NOT NULL, smiley INT NOT NULL, UNIQUE INDEX UNIQ_1323A575DA563A3C (id_chat_session_user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE groupe (id INT AUTO_INCREMENT NOT NULL, libelle VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE message (id INT AUTO_INCREMENT NOT NULL, id_chat_session_user_id INT NOT NULL, texte LONGTEXT NOT NULL, date_send DATETIME NOT NULL, INDEX IDX_B6BD307FDA563A3C (id_chat_session_user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE theme (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE theme_evaluation (theme_id INT NOT NULL, evaluation_id INT NOT NULL, INDEX IDX_80E4993259027487 (theme_id), INDEX IDX_80E49932456C5646 (evaluation_id), PRIMARY KEY(theme_id, evaluation_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, id_groupe_id INT NOT NULL, username VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, status INT NOT NULL, UNIQUE INDEX UNIQ_8D93D649F85E0677 (username), INDEX IDX_8D93D649FA7089AB (id_groupe_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE chat_session_user ADD CONSTRAINT FK_B509BFE279F37AE5 FOREIGN KEY (id_user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE evaluation ADD CONSTRAINT FK_1323A575DA563A3C FOREIGN KEY (id_chat_session_user_id) REFERENCES chat_session_user (id)');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307FDA563A3C FOREIGN KEY (id_chat_session_user_id) REFERENCES chat_session_user (id)');
        $this->addSql('ALTER TABLE theme_evaluation ADD CONSTRAINT FK_80E4993259027487 FOREIGN KEY (theme_id) REFERENCES theme (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE theme_evaluation ADD CONSTRAINT FK_80E49932456C5646 FOREIGN KEY (evaluation_id) REFERENCES evaluation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D649FA7089AB FOREIGN KEY (id_groupe_id) REFERENCES groupe (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE evaluation DROP FOREIGN KEY FK_1323A575DA563A3C');
        $this->addSql('ALTER TABLE message DROP FOREIGN KEY FK_B6BD307FDA563A3C');
        $this->addSql('ALTER TABLE theme_evaluation DROP FOREIGN KEY FK_80E49932456C5646');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649FA7089AB');
        $this->addSql('ALTER TABLE theme_evaluation DROP FOREIGN KEY FK_80E4993259027487');
        $this->addSql('ALTER TABLE chat_session_user DROP FOREIGN KEY FK_B509BFE279F37AE5');
        $this->addSql('DROP TABLE chat_session_user');
        $this->addSql('DROP TABLE evaluation');
        $this->addSql('DROP TABLE groupe');
        $this->addSql('DROP TABLE message');
        $this->addSql('DROP TABLE theme');
        $this->addSql('DROP TABLE theme_evaluation');
        $this->addSql('DROP TABLE user');
    }
}
