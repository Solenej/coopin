<?php

namespace App\Repository;

use App\Entity\UserTheme;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UserTheme|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserTheme|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserTheme[]    findAll()
 * @method UserTheme[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserThemeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UserTheme::class);
    }

    // /**
    //  * @return UserTheme[] Returns an array of UserTheme objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserTheme
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
