<?php

namespace App\Repository;

use App\Entity\Explanation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Explanation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Explanation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Explanation[]    findAll()
 * @method Explanation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExplanationRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Explanation::class);
    }

    // /**
    //  * @return Explanation[] Returns an array of Explanation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Explanation
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
