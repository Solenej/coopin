<?php

namespace App\Repository;

use App\Entity\ChatSessionUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ChatSessionUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method ChatSessionUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method ChatSessionUser[]    findAll()
 * @method ChatSessionUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ChatSessionUserRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ChatSessionUser::class);
    }

    // /**
    //  * @return ChatSessionUser[] Returns an array of ChatSessionUser objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ChatSessionUser
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
