<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserThemeRepository")
 */
class UserTheme
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Evaluation", inversedBy="userThemes")
     */
    private $idEvaluation;

    public function __construct()
    {
        $this->idEvaluation = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Evaluation[]
     */
    public function getIdEvaluation(): Collection
    {
        return $this->idEvaluation;
    }

    public function addIdEvaluation(Evaluation $idEvaluation): self
    {
        if (!$this->idEvaluation->contains($idEvaluation)) {
            $this->idEvaluation[] = $idEvaluation;
        }

        return $this;
    }

    public function removeIdEvaluation(Evaluation $idEvaluation): self
    {
        if ($this->idEvaluation->contains($idEvaluation)) {
            $this->idEvaluation->removeElement($idEvaluation);
        }

        return $this;
    }
}
