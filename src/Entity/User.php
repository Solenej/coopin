<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface, \Serializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $username;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Groupe", inversedBy="users")
     * @ORM\JoinColumn(nullable=false)
     */
    private $idGroupe;

    /**
     * @ORM\Column(type="integer", options={"default" : 0})
     */
    private $status;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ChatSessionUser", mappedBy="idUser")
     */
    private $chatSessionUsers;

    public function __construct()
    {
        $this->chatSessionUsers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getIdGroupe(): ?Groupe
    {
        return $this->idGroupe;
    }

    public function setIdGroupe(?Groupe $idGroupe): self
    {
        $this->idGroupe = $idGroupe;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return Collection|ChatSessionUser[]
     */
    public function getChatSessionUsers(): Collection
    {
        return $this->chatSessionUsers;
    }

    public function addChatSessionUser(ChatSessionUser $chatSessionUser): self
    {
        if (!$this->chatSessionUsers->contains($chatSessionUser)) {
            $this->chatSessionUsers[] = $chatSessionUser;
            $chatSessionUser->setIdUser($this);
        }

        return $this;
    }

    public function removeChatSessionUser(ChatSessionUser $chatSessionUser): self
    {
        if ($this->chatSessionUsers->contains($chatSessionUser)) {
            $this->chatSessionUsers->removeElement($chatSessionUser);
            // set the owning side to null (unless already changed)
            if ($chatSessionUser->getIdUser() === $this) {
                $chatSessionUser->setIdUser(null);
            }
        }

        return $this;
    }

    /** @see \Serializable::serialize() */
    public function serialize() {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            $this->status,
                // see section on salt below
                // $this->salt,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized) {
        list (
                $this->id,
                $this->username,
                $this->password,
                $this->status,
                // see section on salt below
                // $this->salt
                ) = unserialize($serialized);
    }
}
