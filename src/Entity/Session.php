<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SessionRepository")
 */
class Session
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $status;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ChatSessionUser", mappedBy="idSession")
     */
    private $chatSessionUsers;

    /**
     * @ORM\Column(type="string", length=255, options={"default": "STARTED"})
     */
    private $state;

    public function __construct()
    {
        $this->chatSessionUsers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return Collection|ChatSessionUser[]
     */
    public function getChatSessionUsers(): Collection
    {
        return $this->chatSessionUsers;
    }

    public function addChatSessionUser(ChatSessionUser $chatSessionUser): self
    {
        if (!$this->chatSessionUsers->contains($chatSessionUser)) {
            $this->chatSessionUsers[] = $chatSessionUser;
            $chatSessionUser->setIdSession($this);
        }

        return $this;
    }

    public function removeChatSessionUser(ChatSessionUser $chatSessionUser): self
    {
        if ($this->chatSessionUsers->contains($chatSessionUser)) {
            $this->chatSessionUsers->removeElement($chatSessionUser);
            // set the owning side to null (unless already changed)
            if ($chatSessionUser->getIdSession() === $this) {
                $chatSessionUser->setIdSession(null);
            }
        }

        return $this;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(string $state): self
    {
        $this->state = $state;

        return $this;
    }
}
