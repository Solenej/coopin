<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MessageRepository")
 */
class Message
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $texte;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateSend;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ChatSessionUser", inversedBy="messages")
     * @ORM\JoinColumn(nullable=false)
     */
    private $idChatSessionUser;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTexte(): ?string
    {
        return $this->texte;
    }

    public function setTexte(string $texte): self
    {
        $this->texte = $texte;

        return $this;
    }

    public function getDateSend(): ?\DateTimeInterface
    {
        return $this->dateSend;
    }

    public function setDateSend(\DateTimeInterface $dateSend): self
    {
        $this->dateSend = $dateSend;

        return $this;
    }

    public function getIdChatSessionUser(): ?ChatSessionUser
    {
        return $this->idChatSessionUser;
    }

    public function setIdChatSessionUser(?ChatSessionUser $idChatSessionUser): self
    {
        $this->idChatSessionUser = $idChatSessionUser;

        return $this;
    }
}
