<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EvaluationRepository")
 */
class Evaluation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer" , nullable=true)
     */
    private $point;

    /**
     * @ORM\Column(type="text")
     */
    private $text;

    /**
     * @ORM\Column(type="integer" , nullable=true)
     */
    private $smiley;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\ChatSessionUser", inversedBy="evaluation", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $idChatSessionUser;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Theme", mappedBy="idEvaluation")
     */
    private $themes;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\UserTheme", mappedBy="idEvaluation")
     */
    private $userThemes;

    public function __construct()
    {
        $this->themes = new ArrayCollection();
        $this->userThemes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPoint(): ?int
    {
        return $this->point;
    }

    public function setPoint(int $point): self
    {
        $this->point = $point;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getSmiley(): ?int
    {
        return $this->smiley;
    }

    public function setSmiley(int $smiley): self
    {
        $this->smiley = $smiley;

        return $this;
    }

    public function getIdChatSessionUser(): ?ChatSessionUser
    {
        return $this->idChatSessionUser;
    }

    public function setIdChatSessionUser(ChatSessionUser $idChatSessionUser): self
    {
        $this->idChatSessionUser = $idChatSessionUser;

        return $this;
    }

    /**
     * @return Collection|Theme[]
     */
    public function getThemes(): Collection
    {
        return $this->themes;
    }

    public function addTheme(Theme $theme): self
    {
        if (!$this->themes->contains($theme)) {
            $this->themes[] = $theme;
            $theme->addIdEvaluation($this);
        }

        return $this;
    }

    public function removeTheme(Theme $theme): self
    {
        if ($this->themes->contains($theme)) {
            $this->themes->removeElement($theme);
            $theme->removeIdEvaluation($this);
        }

        return $this;
    }

    /**
     * @return Collection|UserTheme[]
     */
    public function getUserThemes(): Collection
    {
        return $this->userThemes;
    }

    public function addUserTheme(UserTheme $userTheme): self
    {
        if (!$this->userThemes->contains($userTheme)) {
            $this->userThemes[] = $userTheme;
            $userTheme->addIdEvaluation($this);
        }

        return $this;
    }

    public function removeUserTheme(UserTheme $userTheme): self
    {
        if ($this->userThemes->contains($userTheme)) {
            $this->userThemes->removeElement($userTheme);
            $userTheme->removeIdEvaluation($this);
        }

        return $this;
    }
}
