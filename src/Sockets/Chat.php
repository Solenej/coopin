<?php
// myapp\src\yourBundle\Sockets\Chat.php;

// Change the namespace according to your bundle, and that's all !
namespace App\Sockets;

use DateTime;
use App\Entity\User;
use App\Entity\Message;
use App\Entity\Session;
use App\Entity\ChatSessionUser;
use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Authentication\Token\JWTUserToken;

class Chat implements MessageComponentInterface {
    protected $clients;
    protected $container;
    protected $doctrine;
    protected $sessionId;

    public function __construct(ContainerInterface $container) {
        $this->clients = new \SplObjectStorage;
        $this->container = $container;
        $this->doctrine = $this->container->get('doctrine');
    }

    public function onOpen(ConnectionInterface $conn) {
        // Store the new connection to send messages to later
        $this->clients->attach($conn);
        $this->sessionId = intval(explode('/', $conn->httpRequest->getUri()->getPath())[2]);

        echo "New connection! ({$conn->resourceId})\n";
    }

    public function onMessage(ConnectionInterface $from, $msg) {
        $totalClients = count($this->clients) - 1;
        echo vsprintf(
            'Connection #%1$d sending message "%2$s" to %3$d other connection%4$s'."\n", [
            $from->resourceId,
            $msg,
            $totalClients,
            $totalClients === 1 ? '' : 's'
        ]);

        $messageData = json_decode(trim($msg));
        if(isset($messageData->token)){
            //1st app message with connected user
            $token_user = $messageData->token;

            //a user auth, else, app sending message auth
            echo "Check user credentials\n";
            //get credentials
            $jwt_manager = $this->container->get('lexik_jwt_authentication.jwt_manager');
            $token = new JWTUserToken();
            $token->setRawToken($token_user);
            $payload = $jwt_manager->decode($token);

            //getUser by username
            // TODO : CHECKER que le user est bien dans la bonne session en plus de l'authentifier
            $user = $this->getUserByUsername($payload['username']);
            if(!$user && $payload["username"] == $user->getUsername()){
                $from->close();
            }
            echo "User found : ".$user->getUsername() . "\n";

            $event = $messageData->event;
            switch($event) {
                case "MESSAGE": 
                    $this->createMessage($messageData->message, $user->getId());
                    break;
                case "PAUSE": 
                    $this->updateChatSessionState("PAUSED");
                    printf("Chat has been paused by " . $user->getUsername());
                    break;
                case "STOP":
                    $this->updateChatSessionState("STOPPED");
                    printf("Chat has been stopped by " . $user->getUsername());
                    break;
                case "RESUME":
                    $this->updateChatSessionState("RESUMED");
                    printf("Chat has been resumed by " . $user->getUsername());
                    break;
                default: break;
            }
            
            foreach ($this->clients as $client) {
                if ($from !== $client) {
                    // The sender is not the receiver, send to each client connected
                    $client->send($msg);
                    echo 'Send message: ' . $msg . "\n";
                }
            }
        } else {
            //error
            $from->send("You're not able to do that!");
        }
    }

    public function onClose(ConnectionInterface $conn) {
        // The connection is closed, remove it, as we can no longer send it messages
        $this->clients->detach($conn);

        echo "Connection {$conn->resourceId} has disconnected\n";
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        echo "An error has occurred: {$e->getMessage()} {$e->getTraceAsString()}\n";

        $conn->close();
    }

    public function createMessage($text, $id)
    {
        $message = new Message();

        $repo = $this->doctrine->getRepository(ChatSessionUser::class);
        $chatSessionUser = $repo->findOneBy(array("idUser" => $id, "idSession" => $this->sessionId));

        if($chatSessionUser && $chatSessionUser instanceof ChatSessionUser) {
            $message->setIdChatSessionUser($chatSessionUser)
            ->setTexte($text)
            ->setDateSend(new \DateTime());
            
            $entityManager = $this->doctrine->getManager();
            $entityManager->persist($message);
            $entityManager->flush();
        }
    }
    
    public function updateChatSessionState($state)
    {
        $sessionRepo = $this->doctrine->getRepository(Session::class);
        $chatSession = $sessionRepo->find($this->sessionId);
        
        if($chatSession && $chatSession instanceof Session){
            $chatSession->setState($state);
            
            $entityManager = $this->doctrine->getManager();
            $entityManager->persist($chatSession);
            $entityManager->flush();
        }
    }
    
    protected function getUserByUsername($username)
    {

        $em = $this->container->get('doctrine')->getManager();
        $repo = $em->getRepository(User::class);

        $user = $repo->findOneBy(array('username' => $username));

        if($user && $user instanceof User){
            return $user;
        } else {
            return false;
        }

    }
}