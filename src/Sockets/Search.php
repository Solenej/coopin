<?php
// myapp\src\yourBundle\Sockets\Chat.php;

// Change the namespace according to your bundle, and that's all !
namespace App\Sockets;

use DateTime;
use App\Entity\User;
use App\Entity\Message;
use App\Entity\Session;
use App\Entity\ChatSessionUser;
use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Authentication\Token\JWTUserToken;

class Search implements MessageComponentInterface {
    protected $clients;
    protected $container;
    protected $doctrine;

    public function __construct(ContainerInterface $container) {
        $this->clients = new \SplObjectStorage;
        $this->container = $container;
        $this->doctrine = $this->container->get('doctrine');
    }

    public function onOpen(ConnectionInterface $conn) {
        // Store the new connection to send messages to later
        $this->clients->attach($conn, "");

        echo "New connection! ({$conn->resourceId})\n";
    }

    public function onMessage(ConnectionInterface $from, $msg) {
        $totalClients = count($this->clients) - 1;
        echo vsprintf(
            'Connection #%1$d sending message "%2$s" to %3$d other connection%4$s'."\n", [
            $from->resourceId,
            $msg,
            $totalClients,
            $totalClients === 1 ? '' : 's'
        ]);

        $messageData = json_decode(trim($msg));
        if(isset($messageData->token)){
            //1st app message with connected user
            $token_user = $messageData->token;

            //a user auth, else, app sending message auth
            echo "Check user credentials\n";
            //get credentials
            $jwt_manager = $this->container->get('lexik_jwt_authentication.jwt_manager');
            $token = new JWTUserToken();
            $token->setRawToken($token_user);
            $payload = $jwt_manager->decode($token);

            //getUser by username
            // TODO : CHECKER que le user est bien dans la bonne session en plus de l'authentifier
            $user = $this->getUserByUsername($payload['username']);
            if(!$user && $payload["username"] == $user->getUsername()){
                $from->close();
            }
            echo "User found : ".$user->getUsername() . "\n";

            switch($messageData->event) {
                case "SEARCH": 
                    $this->search($from, $user);
                    break;
                case "MATCHED":
                    break;
                default: break;
            }
        } else {
            //error
            $from->send("You're not able to do that!");
        }
    }

    public function onClose(ConnectionInterface $conn) {
        // The connection is closed, remove it, as we can no longer send it messages
        if($this->clients[$conn]){
            if($user = $this->getUserByUsername($this->clients[$conn])) {
                $this->unqueue($user);
            }
        }
        
        $this->clients->detach($conn);

        echo "Connection {$conn->resourceId} has disconnected\n";
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        echo "An error has occurred: {$e->getMessage()} {$e->getTraceAsString()}\n";

        $conn->close();
    }

    protected function search(ConnectionInterface $from, User $user) {
        if ($user->getStatus() == 0) {
            $matchedUser = $this->searchOneUser($user);

            // Si c'est le cas, on crée 1 session et 2 chatSessionUser pour les 2 utilisateurs
            if (!empty($matchedUser)) {
                $session = $this->createSession();
                $chatSessionUser = $this->createChatSessionUser($user, $session);
                $chatSessionMatchedUser = $this->createChatSessionUser($matchedUser, $session);

                // On renvoie les informations pour permettre la redirection de l'utilisateur connecté
                $this->clients->rewind();
                while($this->clients->valid()) {
                    if ($this->clients->getInfo() == $matchedUser->getUsername()) {

                        echo "Matched! ".$user->getUsername() . " with " . $matchedUser->getUsername() . "\n";
                        // Envoie l'event de match au user matched
                        $msg = json_encode(array("event" => "MATCHED", "session_id" => $session->getId(), "chatSessionUserId" => $chatSessionMatchedUser->getId()));
                        $this->clients->current()->send($msg);

                        echo 'Send message to' . $chatSessionMatchedUser->getIdUser()->getUsername() . ' : ' . $msg . "\n";
                        
                        // Envoie l'event au user de la connexion courrante
                        $msg = json_encode(array("event" => "MATCHED", "session_id" => $session->getId(), "chatSessionUserId" => $chatSessionUser->getId()));
                        $from->send($msg);
                        
                        echo 'Send message to' . $chatSessionUser->getIdUser()->getUsername() . ' : ' . $msg . "\n";
                    }
                    $this->clients->next();
                }

            } else {
                // Si aucun utilisateur recherche un match, on update l'utilisateur en recherche au status = 1
                $user->setStatus(1);

                $em = $this->doctrine->getManager();
                $em->persist($user);
                $em->flush();
                

                foreach ($this->clients as $client) {
                    if ($from == $client) {
                        $this->clients[$from] = $user->getUsername();
                        echo "No match. Add " . $user->getUsername() . " to waiting list." . "\n";
                    }
                }
            }
        } else {
            foreach ($this->clients as $client) {
                if ($from == $client) {
                    $this->clients[$from] = $user->getUsername();
                    echo "No match. Add " . $user->getUsername() . " to waiting list." . "\n";
                }
            }
        }
    }
    
    // Permet de rechercher un utilisateur connecté afin de match
    public function searchOneUser(User $user)
    {
        $user_groupe = $user->getIdGroupe()->getId();
        $user_username = $user->getUsername();

        $em = $this->doctrine->getManager();
        $query = "SELECT *
                FROM user
                WHERE id_groupe_id = $user_groupe
                AND status = 1
                AND username != '$user_username'
                ORDER BY RAND()
                LIMIT 1";

        $statement = $em->getConnection()->prepare($query);
        $statement->execute();

        $result = $statement->fetchAll();

        // Permet de convertir le resultat en objet User
        if (!empty($result)) {
            $user = $em->getRepository(User::class)->find($result[0]['id']);
            return $user;
        }

        return $result;
    }

    // Permet de créer une session
    public function createSession() {
        $session = new Session();
        $session->setStatus(1);
        $session->setState("STARTED");

        $em = $this->doctrine->getManager();
        $em->persist($session);
        $em->flush();

        return $session;
    }

    // Permet de créer une chat session user à partir d'un user et d'une session
    public function createChatSessionUser($user, $session) {
        $chatSessionUser = new ChatSessionUser();
        $chatSessionUser->setIdUser($user);

        $chatSessionUser->setIdSession($session);
        $chatSessionUser->setStatus(0);
        $user->setStatus(0);

        $em = $this->doctrine->getManager();
        $em->persist($chatSessionUser);
        $em->persist($user);
        $em->flush();

        return $chatSessionUser;
    }

    // Retire le status de recherche active sur l'utilisateur en bdd
    public function unqueue($user) {
        $user->setStatus(0);

        $em = $this->doctrine->getManager();
        $em->persist($user);
        $em->flush();
        echo "User " . $user->getUsername() . " has been unqueued from search \n";
        
        return true;
    }

    protected function getUserByUsername($username)
    {

        $em = $this->doctrine->getManager();
        $repo = $em->getRepository(User::class);

        $user = $repo->findOneBy(array('username' => $username));

        if($user && $user instanceof User){
            return $user;
        } else {
            return false;
        }

    }
}