<?php

namespace App\Controller\Rest;

use DateTime;
use App\Entity\User;
use App\Entity\Message;
use App\Entity\Session;
use App\Entity\ChatSessionUser;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\JsonResponse;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Authentication\Token\JWTUserToken;


class ChatController extends FOSRestController {

    /**
     * @Rest\Get(
     *    path = "/chatSession/{id}/messages",
     *    name = "api_chat_messages"
     * )
     * @Rest\View(StatusCode = 200)
     * @RequestParam(name="id", requirements="\d+", default="", description="Chat session user id")
     * 
     */
    public function listMessages(Request $request) {

        $em = $this->getDoctrine()->getManager();
        
        $id= $request->get('id');

        $query = " SELECT u.username, m.texte, m.date_send
                    FROM message m, chat_session_user c, user u
                    WHERE m.id_chat_session_user_id = c.id
                    AND c.id_user_id = u.id
                    AND c.id_session_id = $id
                    ORDER BY m.date_send ";

        $statement = $em->getConnection()->prepare($query);
        $statement->execute();

        $result = $statement->fetchAll();

        return new JsonResponse($result);
    }

    /**
     * @Rest\Get(
     *    path = "/chatSession",
     *    name = "api_user_paused_chat"
     * )
     * @Rest\View(StatusCode = 200)
     * 
     */
    public function getUserChatSession(Request $request)
    {
        $user = $this->getUser();
        if(! $chatSession = $this->getSessionByUser($user->getId()))
        {
            return new JsonResponse(false, Response::HTTP_NOT_FOUND);
        }
        return new JsonResponse($chatSession);
    }
    
    /**
     * @Rest\Get(
     *    path = "/chatSession/{id}/createMessage",
     *    name = "api_user_send_message"
     * )
     * @Rest\View(StatusCode = 201)
     * 
     */
    public function createChatMessage(Request $request) {
        $user = $this->getUser();

        //getUser by username
        // TODO : CHECKER que le user est bien dans la bonne session en plus de l'authentifier
        $msg = $request->query->get("messsage");
        $sessionId = $request->get("id");
        
        if(!$succeed = $this->createMessage($msg, $user->getId(), $sessionId)) {

            return new JsonResponse(array("error" => "Session or user not found"), Response::HTTP_NOT_FOUND);
        }
        
        return new JsonResponse(array("username" => $user->getUsername()));
    }
    
    /**
     * @Rest\Get(
     *    path = "/chatSession/{id}/changeState",
     *    name = "api_user_change_state"
     * )
     * @Rest\View(StatusCode = 200)
     * 
     */
    public function changeChatState(Request $request) {
        $user = $this->getUser();

        //getUser by username
        // TODO : CHECKER que le user est bien dans la bonne session en plus de l'authentifier
        $state = $request->query->get("state");
        $sessionId = $request->get("id");
        
        if(!$updated = $this->updateChatSessionState($state, $sessionId)) {
            return new JsonResponse(array("error" => "Session not found"), Response::HTTP_NOT_FOUND);
        }
        
        return new JsonResponse(true);
    }
    

    // On récupère la dernière session non évaluée ou en cours
    protected function getSessionByUser($userId) {
        $chatSessionUserRepo = $this->getDoctrine()->getRepository(ChatSessionUser::class);
        $chatSessionUser = $chatSessionUserRepo->findOneBy(array("idUser" => $userId, "status" => 0), array("id" => "DESC"));

        if($chatSessionUser && $chatSessionUser instanceof ChatSessionUser) {

            $sessionRepo = $this->getDoctrine()->getRepository(Session::class);
            $chatSession = $sessionRepo->findOneBy(array("id" => $chatSessionUser->getIdSession()));
    
            if($chatSession && $chatSession instanceof Session) {
                return array("chatSessionUserId" => $chatSessionUser->getId(), "sessionId" => $chatSession->getId(), "state" => $chatSession->getState());
            }
        }

        return false;
    }

    public function createMessage($text, $id, $sessionId)
    {
        $message = new Message();

        $repo = $this->getDoctrine()->getRepository(ChatSessionUser::class);
        $chatSessionUser = $repo->findOneBy(array("idUser" => $id, "idSession" => $sessionId));

        if($chatSessionUser && $chatSessionUser instanceof $chatSessionUser) {
            $message->setIdChatSessionUser($chatSessionUser)
            ->setTexte($text)
            ->setDateSend(new \DateTime());
            
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($message);
            $entityManager->flush();

            return true;
        }
        return false;
    }
    
    public function updateChatSessionState($state, $sessionId)
    {
        $sessionRepo = $this->getDoctrine()->getRepository(Session::class);
        $chatSession = $sessionRepo->find($sessionId);
        
        if($chatSession && $chatSession instanceof Session) {
            $chatSession->setState($state);
            
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($chatSession);
            $entityManager->flush();

            return true;
        }
        return false;
    }

    protected function getUserByUsername($username)
    {

        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository(User::class);

        $user = $repo->findOneBy(array('username' => $username));

        if($user && $user instanceof User){
            return $user;
        } else {
            return false;
        }

    }
}