<?php

namespace App\Controller\Rest;

use App\Entity\User;
use App\Entity\Session;
use App\Entity\ChatSessionUser;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\JsonResponse;


class MatchController extends FOSRestController
{
     /**
     * @Rest\Get(
     *    path = "/search",
     *    name = "api_search"
     * )
     * @Rest\View(StatusCode = 200)
     */
    public function search() {
        $user = $this->getUser();

        // Si l'utilisateur est au status 0, on regarde si un autre utilisateur est au status 1
        if ($user->getStatus() == 0) {
            $matchedUser = $this->searchOneUser();

            // Si c'est le cas, on crée 1 session et 2 chatSessionUser pour les 2 utilisateurs
            if (!empty($matchedUser)) {
                $session = $this->createSession();
                $chatSessionUser = $this->createChatSessionUser($user, $session);
                $chatSessionMatchedUser = $this->createChatSessionUser($matchedUser, $session);

                // On renvoie les informations pour permettre la redirection de l'utilisateur connecté
                return new JsonResponse([
                    'status' => 'MATCHED',
                    'session_id' => $session->getId(),
                    'chatSessionUserId' => $chatSessionUser->getId(),
                    'chatSessionMatchedUserId' => $chatSessionMatchedUser->getId(),
                    'matched_username' => $matchedUser->getUsername()
                ]);
            } else {
                // Si aucun utilisateur recherche un match, on update l'utilisateur en recherche au status = 1
                $user->setStatus(1);

                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();

                return new JsonResponse(array("username" => $user->getUsername(), "status" => "PENDING"));
            }
        } else {
            return new JsonResponse(array("username" => $user->getUsername(), "status" => "PENDING"));
        }
    }

     /**
     * @Rest\Get(
     *    path = "/unqueue",
     *    name = "api_unqueue"
     * )
     * @Rest\View(StatusCode = 200)
     */
    public function unenqueue() {
        $user = $this->getUser();

        $user->setStatus(0);

        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        return new JsonResponse(array("username" => $user->getUsername()));
    }

    // Permet de rechercher un utilisateur connecté afin de match
    public function searchOneUser()
    {
        $user_groupe = $this->getUser()->getIdGroupe()->getId();
        $user_username = $this->getUser()->getUsername();

        $em = $this->getDoctrine()->getManager();
        $query = "SELECT *
               FROM user
               WHERE id_groupe_id = $user_groupe
               AND status = 1
               AND username != '$user_username'
               ORDER BY RAND()
                LIMIT 1";

       $statement = $em->getConnection()->prepare($query);
       $statement->execute();

       $result = $statement->fetchAll();

       // Permet de convertir le resultat en objet User
       if (!empty($result)) {
           $user = $em->getRepository(User::class)->find($result[0]['id']);
           return $user;
       }

       return $result;
    }

    // Permet de créer une session
    public function createSession() {
        $session = new Session();
        $session->setStatus(1);
        $session->setState("STARTED");

        $em = $this->getDoctrine()->getManager();
        $em->persist($session);
        $em->flush();

        return $session;
    }

    // Permet de créer une chat session user à partir d'un user et d'une session
    public function createChatSessionUser($user, $session) {
        $chatSessionUser = new ChatSessionUser();
        $chatSessionUser->setIdUser($user);

        $chatSessionUser->setIdSession($session);
        $chatSessionUser->setStatus(0);
        $user->setStatus(0);

        $em = $this->getDoctrine()->getManager();
        $em->persist($chatSessionUser);
        $em->persist($user);
        $em->flush();

        return $chatSessionUser;
    }

    // Permet de vérifier si un utilisateur est déjà dans une session avec un autre utilisateur
    public function getSession($user) {
        $user_id = $user->getId();
        $em = $this->getDoctrine()->getManager();

        $query = "SELECT s.id, c.id as chatSessionUserId
               FROM chat_session_user c
               JOIN session s ON c.id_session_id = s.id AND s.state = 'STARTED' OR s.state = 'PAUSED'
               WHERE id_user_id = '$user_id'
               AND s.status = 1
               AND c.status = 0";


        $statement = $em->getConnection()->prepare($query);
        $statement->execute();

        $result = $statement->fetchAll();

        if (count($result) >= 1 ) {
            $session = $em->getRepository(Session::class)->find($result[0]['id']);
            return $session;
        }

       return false;
    }

}
