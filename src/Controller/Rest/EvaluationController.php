<?php

namespace App\Controller\Rest;

use App\Entity\Theme;
use App\Entity\UserTheme;
use App\Entity\Evaluation;
use App\Entity\ChatSessionUser;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\JsonResponse;
use FOS\RestBundle\Controller\Annotations\RequestParam;

class EvaluationController extends FOSRestController
{
  /**
     * @Rest\Post(
     *    path = "/newEvaluation",
     *    name = "api_newEvaluation"
     * )
     * @Rest\View(StatusCode = 200)
     * @RequestParam(name="newText", requirements="\d+", default="", description="Group name")
     */
    public function newEvaluation(Request $request) {
        $entityManager = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        $content = json_decode($request->getContent());

        $repo = $this->getDoctrine()->getRepository(ChatSessionUser::class);
        $chatSessionUser = $repo->find($content->evaluation->chatSessionUser);

        $repo = $this->getDoctrine()->getRepository(Evaluation::class);
        $evaluationChatSessionUser = $repo->findOneBy(array('idChatSessionUser' => $chatSessionUser));

        if (empty($evaluationChatSessionUser)) {

            $evaluation = new Evaluation();
            $evaluation->setIdChatSessionUser($chatSessionUser);
            $evaluation->setPoint($content->evaluation->stars);
            $evaluation->setText($content->evaluation->comment);
            $evaluation->setSmiley($content->evaluation->feeling);
            $entityManager->persist($evaluation);

            $chatSessionUser->setStatus(1);
            $entityManager->persist($chatSessionUser);
            $entityManager->flush();

            $existingHashtags = $content->evaluation->hashtags;
            foreach ($existingHashtags as $name => $boolean) {
                if ($boolean) {
                    $repo = $this->getDoctrine()->getRepository(Theme::class);
                    $existingTheme = $repo->findOneBy(array('name' => $name));

                    $existingTheme->addIdEvaluation($evaluation);
                    $entityManager->persist($existingTheme);
                    $entityManager->flush();
                }
            }

            $otherHashtags = $content->evaluation->otherHashtags;
            foreach ($otherHashtags as $name) {
                $repo = $this->getDoctrine()->getRepository(UserTheme::class);
                $existingUserTheme = $repo->findOneBy(array('name' => $name));
                if (!empty($existingUserTheme)) {
                    $existingUserTheme->addIdEvaluation($evaluation);
                    $entityManager->persist($existingUserTheme);
                } else {
                    if ($name != NULL) {
                        $userTheme = new UserTheme();
                        $userTheme->setName($name);
                        $userTheme->addIdEvaluation($evaluation);
                        $entityManager->persist($userTheme);
                    }
                }
                $entityManager->flush();
            }

            $entityManager->flush();
            return new JsonResponse(['ok' => 'L\'évaluation a été enregistrée.']);
        } else {
            // Si l'evaluation existe déjà, on renvoie false
            return new JsonResponse(['error' => 'L\'évaluation existe déjà']);
        }
    }
}
