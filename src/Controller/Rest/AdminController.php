<?php

namespace App\Controller\Rest;

use App\Entity\User;
use App\Entity\Theme;
use App\Entity\Groupe;
use App\Entity\Explanation;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\JsonResponse;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AdminController extends FOSRestController
{

    /**
     * @Rest\Get(
     *    path = "/listGroups",
     *    name = "api_admin_listGroups"
     * )
     * @Rest\View(StatusCode = 200)
     */
    public function listGroups() {
        $repo = $this->getDoctrine()->getRepository(Groupe::class);

        $groups = $repo->findAll();

        if (!empty($groups)) {
            $groupsJson = array();
            foreach ($groups as $group) {
                $tmp = array();
                $tmp['value'] = $group->getLibelle();
                $tmp['label'] = $group->getLibelle();
                array_push($groupsJson, $tmp);
            }

            return new JsonResponse(['ok' => $groupsJson]);
        } else {
            return new JsonResponse(['error' => 'La liste de groupe est vide.']);
        }
    }

        /**
     * @Rest\Get(
     *    path = "/listUsersGroups",
     *    name = "api_admin_listUsersGroups"
     * )
     * @Rest\View(StatusCode = 200)
     */
    public function getUserGroup() {
        $em = $this->getDoctrine()->getManager();
        
        $query = " SELECT u.id, u.username, u.roles, g.libelle 
                FROM user u, groupe g
                WHERE u.id_groupe_id = g.id";

        $statement = $em->getConnection()->prepare($query);
        $statement->execute();

        $result = $statement->fetchAll();

        if (!empty($result)) {
            return new JsonResponse(['ok' => $result]);
        } else {
            return new JsonResponse(['error' => 'La liste de groupe est vide.']);
        }
    }

     /**
     * @Rest\Get(
     *    path = "/listUsers",
     *    name = "api_admin_listUsers"
     * )
     * @Rest\View(StatusCode = 200)
     */
    public function listUsers() {
        $repo = $this->getDoctrine()->getRepository(User::class);

        $users = $repo->findAll();

        if (!empty($users)) {
            $usersJson = array();
            foreach ($users as $user) {
                $tmp = array();
                $tmp['value'] = $user->getUsername();
                $tmp['label'] = $user->getUsername();
                array_push($usersJson, $tmp);
            }

            return new JsonResponse(['ok' => $usersJson]);
        } else {
            return new JsonResponse(['error' => 'La liste de user est vide.']);
        }
    }
    /**
     * @Rest\Get(
     *    path = "/listThemes",
     *    name = "api_admin_listThemes"
     * )
     * @Rest\View(StatusCode = 200)
     */
    public function listThemes() {
        $repo = $this->getDoctrine()->getRepository(Theme::class);

        $themes = $repo->findAll();

        if (!empty($themes)) {
            $themesJson = array();
            foreach ($themes as $theme) {
                $tmp = array();
                $tmp['id'] = $theme->getId();
                $tmp['nom'] = $theme->getName();
                array_push($themesJson, $tmp);
            }

            return new JsonResponse(['ok' => $themesJson]);
        } else {
            return new JsonResponse(["erro" => "Il n'y a aucun thème."]);
        }
    }

    /**
     * @Rest\Get(
     *    path = "/getExplanation",
     *    name = "api_admin_getExplanation"
     * )
     * @Rest\View(StatusCode = 200)
     */
    public function getExplanation() {
        $repo = $this->getDoctrine()->getRepository(Explanation::class);

        $actualExplanation = $repo->findOneBy([], ['id' => 'desc']);

        if ($actualExplanation == NULL) {
            return new JsonResponse(['value' => '', 'label' => 'actualExplanation']);
        } else {
            $actualExplanationJson = array();
            $actualExplanationJson['value'] = $actualExplanation->getText();
            $actualExplanationJson['label'] = 'actualExplanation';

            return new JsonResponse($actualExplanationJson);
        }
    }

    /**
     * @Rest\Post(
     *    path = "/newGroup",
     *    name = "api_admin_newGroup"
     * )
     * @Rest\View(StatusCode = 200)
     * @RequestParam(name="newGroup", requirements="\d+", default="", description="Group name")
     */
    public function newGroup(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $existingGroup = $em->getRepository(Groupe::class)->findBy(['libelle' => $request->get('_nameGroup')]);

        if (!empty($existingGroup)) {
            return new JsonResponse(['error' => 'Ce groupe existe déjà.']);
        } else {
            $group = new Groupe();
            $group->setLibelle($request->get('_nameGroup'));

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($group);
            $entityManager->flush();

            return new JsonResponse(['ok' => 'Le groupe a bien été créée.']);
        }
    }

    /**
     * @Rest\Post(
     *    path = "/newTheme",
     *    name = "api_admin_newTheme"
     * )
     * @Rest\View(StatusCode = 200)
     * @RequestParam(name="newTheme", requirements="\d+", default="", description="Theme name")
     */
    public function newTheme(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $existingTheme = $em->getRepository(Theme::class)->findBy(['name' => $request->get('_nameTheme')]);

        if (!empty($existingTheme)) {
            return new JsonResponse(['error' => 'Ce thème existe déjà.']);
        } else {
            $theme = new Theme();
            $theme->setName($request->get('_nameTheme'));

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($theme);
            $entityManager->flush();

            return new JsonResponse(['ok' => 'Le thème a bien été créée.']);
        }
    }

    /**
     * @Rest\Post(
     *    path = "/newUser",
     *    name = "api_admin_newUser"
     * )
     * @Rest\View(StatusCode = 200)
     * @RequestParam(name="codeAccess", requirements="\d+", default="", description="User access code")
     * @RequestParam(name="nameGroup", requirements="\d+", default="", description="User group name")
     * @RequestParam(name="checkedAdmin", default="", description="User role type")
     */
    public function newUser(Request $request, UserPasswordEncoderInterface $encoder) {
        $repo = $this->getDoctrine()->getRepository(User::class);
        $existingUser = $repo->findBy(['username' => $request->get('_codeAccess')]);

        if (!empty($existingUser)) {
            return new JsonResponse(['error' => 'Cet utilisateur existe déjà.']);
        } else {
            $user = new User();
            $user->setUsername($request->get('_codeAccess'));


            $repo = $this->getDoctrine()->getRepository(Groupe::class);
            $grp = $repo->findOneBy(array('libelle' => $request->get('_nameGroup')));
            $user->setIdGroupe($grp);

            $encoded = $encoder->encodePassword($user, $request->get('_codeAccess'));
            $user->setPassword($encoded);
            if($request->get('_checkAdmin') === true) {
                $user->setRoles(["ROLE_ADMIN"]);
            } else {
                $user->setRoles(["ROLE_USER"]);
            }
            $user->setStatus(0);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return new JsonResponse(['ok' => 'L\'utilisateur a bien été créée.']);
        }
    }

    /**
     * @Rest\Post(
     *    path = "/newText",
     *    name = "api_admin_newText"
     * )
     * @Rest\View(StatusCode = 200)
     * @RequestParam(name="_textExplanation", requirements="\d+", default="", description="Explanation text")
     */
    public function newText(Request $request) {
        $repo = $this->getDoctrine()->getRepository(Explanation::class);
        $existingExplanation = $repo->findBy(['text' => $request->get('_textExplanation')]);

        if (!empty($existingExplanation)) {
            return new JsonResponse(['error' => 'Le texte est similaire au texte actuel.']);
        } else {
            $explanation = new Explanation();
            $explanation->setText($request->get('_textExplanation'));

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($explanation);
            $entityManager->flush();

            return new JsonResponse(['ok' => 'Le texte a bien été enregistré.']);
        }
    }

     /**
     * @Rest\Get(
     *    path = "/newExport",
     *    name = "api_admin_newExport"
     * )
     * @Rest\View(StatusCode = 200)
     */
    public function exportBdd()
    {

        $chemin = $this->getParameter('download').'/exportCoopin';
        if (!file_exists($chemin)) {
            mkdir($chemin, 0777, true) ;
        }


        $zip = $this->getZipArchive();
        $zip->open($chemin.'.zip', \ZipArchive::CREATE);

        $evaluation = $this->exportEvaluations();

        $this->writeInCsv($chemin.'/evaluation.csv', $evaluation['elementToExport'], $evaluation['colonneEvaluationToExport']);
        $zip->addFile($chemin.'/evaluation.csv' , 'evaluation.csv');


        $messages = $this->exportMessages();

        $this->writeInCsv($chemin.'/messages.csv', $messages['elementToExport'], $messages['colonneEvaluationToExport']);
        $zip->addFile($chemin.'/messages.csv' , 'messages.csv');


        return new JsonResponse(true);
    }


    protected function exportEvaluations()
    {

        $elementToExport = [];
        $evaluations = $this->getEvaluation();

        foreach($evaluations as $evaluation){


            $id = $evaluation['id'];

            $resultThemesParDefault = $this->getThemePardefault($id);
            $resultThemesUser = $this->getThemeUser($id);

            $resultThemes = array_merge( $resultThemesParDefault, $resultThemesUser);

            $evaluation = $this->setUpDataExport($resultThemes , $evaluation);

            array_push($elementToExport , $evaluation);
        }

        $colonneEvaluationToExport = [
            'Login' , 'Note de l\'application', 'Texte de fin de chat', 'Smiley' , 'Liste des thèmes',
        ];

        return[
            'elementToExport'           => $elementToExport,
            'colonneEvaluationToExport' => $colonneEvaluationToExport,
        ];
    }

    protected function exportMessages()
    {

        $messages = $this->getMessages();


        $colonneEvaluationToExport = [
            'Login' , 'Message', 'Date d\'envoie' , 'Chat Session'
        ];

        return[
            'elementToExport'           => $messages,
            'colonneEvaluationToExport' => $colonneEvaluationToExport,
        ];

    }

    protected function setUpDataExport($resultThemes , $evaluation)
    {
        $nbTheme = 1 ;

        foreach($resultThemes as $resultTheme){
            $evaluation["theme_$nbTheme"] = $resultTheme['name'] ;
            $nbTheme++;
        }

        unset($evaluation['id']);

        return $evaluation ;
    }


    /**
     * @param $id
     */
    protected function getThemePardefault($id)
    {
        $em = $this->getDoctrine()->getManager();
        $query = "  SELECT  t.name
                    FROM  theme t, theme_evaluation te
                    WHERE te.theme_id = t.id
                    AND te.evaluation_id = $id";


        $statement = $em->getConnection()->prepare($query);
        $statement->execute();

        return $statement->fetchAll();

    }

    /**
     * @param $id
     */
    protected function getThemeUser($id)
    {
        $em = $this->getDoctrine()->getManager();
        $query = "  SELECT ut.name
                    FROM  user_theme ut, user_theme_evaluation ute
                    WHERE ut.id = ute.user_theme_id
                    AND ute.evaluation_id = $id";


        $statement = $em->getConnection()->prepare($query);
        $statement->execute();

        return $statement->fetchAll();

    }

    /**
     *
     */
    protected function getEvaluation()
    {
        $em = $this->getDoctrine()->getManager();
        $query = "  SELECT u.username, e.id, point, text, smiley
                    FROM evaluation e, user u, chat_session_user csu
                    WHERE e.id_chat_session_user_id = csu.id
                    AND u.id = csu.id_user_id ";


        $statement = $em->getConnection()->prepare($query);
        $statement->execute();

        return $statement->fetchAll();

    }

        /**
     *
     */
    protected function getMessages()
    {
        $em = $this->getDoctrine()->getManager();
        $query = "  SELECT u.username, texte , date_send , csu.id
                    FROM  user u, chat_session_user csu , message m
                    WHERE m.id_chat_session_user_id = csu.id
                    AND u.id = csu.id_user_id
                    ORDER BY username ";


        $statement = $em->getConnection()->prepare($query);
        $statement->execute();

        return $statement->fetchAll();

    }


    /**
     * @param $chemin
     * @param $elements
     * @param $listeColonne
     */
    protected function writeInCsv($chemin, $elements, $listeColonne)
    {

        $fp = fopen($chemin, 'w');

        fputcsv($fp, $listeColonne, ";");

        foreach ($elements as $element) {
            fputcsv($fp, $element, ";");
        }

        fclose($fp);
    }


    protected function getZipArchive()
    {
        return new \ZipArchive();
    }
}
