<?php

namespace App\Controller\Web;

use App\Entity\Message;
use App\Entity\ChatSessionUser;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\JsonResponse;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class ChatController extends FOSRestController
{
    /**
     * @Route("/chatSessionTest/{id}", name="show_chat_session_user")
     */
    public function showChatSessionUser(Request $request, $id, ObjectManager $manager){
        return $this->render('chat/chat.html.twig');
    }
    
    /**
     * @Route("/searchTest", name="search")
     */
    public function search(Request $request, ObjectManager $manager){
        return $this->render('search/search.html.twig');
    }
}
