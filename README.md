# Prérequis

`Version de PHP 7.1.3`

1. Composer
`brew install composer`
Une fois installé
`composer install`

2. Yarn ou NPM
Une fois installé
```bash
yarn install
yarn run webpack-dev
yarn run webpack-serverside
```

3. Créer des clés publiques et privées pour l'authentification via JWT Tokens

```bash
mkdir config/jwt
openssl genrsa -out config/jwt/private.pem -aes256 4096
openssl rsa -pubout -in config/jwt/private.pem -out config/jwt/public.pem
```
*Pour windows :*
* Installer OpenSSL [Télécharger](https://indy.fulgan.com/SSL/)
* Extraire et déplacer les fichiers dans C:/OpenSSL-Win64
* Exécuter les commandes à la racines du projet
```bash
mkdir config/jwt
& 'C:\OpenSSL-Win64\bin\openssl.exe' genrsa -out config/jwt/private.pem -aes256 4096
& 'C:\OpenSSL-Win64\bin\openssl.exe' rsa -pubout -in config/jwt/private.pem -out
```

Ajouter dans votre .env.local `JWT_PASSPHRASE=<votre_passphrase>`

# Lancer le serveur de développement

```bash
php bin/console server:run
```

http://localhost:8000/ ou http://127.0.0.1:8000/

# Remettre à jour la BDD

1. En ligne de commande :
```bash
php bin/console doctrine:database:drop --force
php bin/console doctrine:database:create
php bin/console doctrine:migrations:migrate
```

# Créer un utilisateur et se connecter

1. Se rendre sur localhost/phpmyadmin

2. Accéder à la base de données (coopin) puis à la table User

3. Dans l'onglet SQL :

```sql
INSERT INTO `groupe` (`id`, `libelle`) VALUES (1, 'Groupe A');
INSERT INTO `user` (`id_groupe_id`, `username`, `roles`, `password`, `status`) VALUES
(1, 'aze', '[\"ROLE_USER\"]', '$2y$13$YrEXjcveTd/heGXorGhlsuveohxQlOydUCODa8HzwZb8xXq1P/Xcm', 0);
```

Pour role admin :

```sql
INSERT INTO `groupe` (`id`, `libelle`) VALUES (1, 'Groupe A');
INSERT INTO user (id_groupe_id, username, roles, password, status) VALUES
(1, 'admin', '["ROLE_ADMIN"]', '$2y$13$D2JpIhlkCqiek4Yh7P4T.OI5bKHlYWCiwsWe94Nc3MDc7.460trB2', 0);
```

Si vous voulez créer un autre utilisateur, vous devrez hasher le mdp. Pour ceci, en ligne de commande :
`php bin/console security:encode-password`
Entrez le mot de passe que vous voulez hasher. Le mot de passe est la value de <Encoded password>

4. Pour connecter l'utilisateur et deconnecter :
http://localhost:8000/login
Username : aze
Password : aze

5. Retourner sur /login pour se déconnecter
http://localhost:8000/login

# Connexion BDD 

1. Créer un .env.local
2. Créer la variable DATABASE_URL (Voir .env.example)
2. Créer la variable WS_BASE (ici ws://localhost:8080 en local)
2. Lancer MAMP/LAMP/WAMP
3. Lancer la commande php bin/console doctrine:database:create
4. Lancer la commande php bin/console doctrine:migration:migrate
